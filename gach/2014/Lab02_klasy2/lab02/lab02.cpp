// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze zaimplementowac klasy LiczbaZespolona i
// Wektor. LiczbaZespolona jest to para liczb, wypisywana w nawiasie
// okraglym. Wektor jest to uporzadkowany zbior trzech
// LiczbZespolonych.

// Proszę zadbać o poprawne zarzadzanie pomiecia.

// Pliku lab02.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab02. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_02

#include "lab02.h"
#include <iostream>

int main ()
{
  using namespace std;

  LiczbaZespolona liczba (1.2, 2.2);
  liczba.Wypisz();
  cout<<"\n";
  
  Wektor* pierwszy = new Wektor;
  pierwszy->UstawX(1.1, 2.1).UstawY(1.2, 2.2).UstawZ(1.3, 2.3);
  pierwszy->Wypisz();

  // zmiana pierwszej wspolrzednej na 8
  pierwszy->WspolrzednaX() = LiczbaZespolona(5.5, 6.6);
  pierwszy->Wypisz();

  // przygotowanie tablicy wspolrzednych wektora
  const int wymiarWektora = 3;
  const Wektor *drugi = Wektor::UtworzConstWektor (LiczbaZespolona(11.1, 12.1), LiczbaZespolona(11.2, 12.2), LiczbaZespolona(11.3, 12.3)); // tworzy wektor o zadanym rozmiarze i wartosciach wspolrzednych
  drugi->Wypisz();

  cout<<"Wspolrzedna X drugiego wektora: ";
  drugi->WspolrzednaX().Wypisz();
  cout<<"\n";

  pierwszy->Posprzataj();
}
/* wynik dzialania programu:
(1.2, 2.2)
[(1.1, 2.1), (1.2, 2.2), (1.3, 2.3)]
[(5.5, 6.6), (1.2, 2.2), (1.3, 2.3)]
[(11.1, 12.1), (11.2, 12.2), (11.3, 12.3)]
Wspolrzedna X drugiego wektora: (11.1, 12.1)
*/
