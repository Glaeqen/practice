#pragma once

#include "ComplexNumber.h"

class Wektor{
private:
	LiczbaZespolona _xCoord;
	LiczbaZespolona _yCoord;
	LiczbaZespolona _zCoord;
public:
	Wektor(){}
	Wektor(const LiczbaZespolona& xCoord, const LiczbaZespolona& yCoord, const LiczbaZespolona& zCoord):
	_xCoord(xCoord),
	_yCoord(yCoord),
	_zCoord(zCoord){}
	static Wektor *UtworzConstWektor(const LiczbaZespolona& xCoord, const LiczbaZespolona& yCoord, const LiczbaZespolona& zCoord);

	Wektor& UstawX(double realPart, double imaginaryPart);
	Wektor& UstawY(double realPart, double imaginaryPart);
	Wektor& UstawZ(double realPart, double imaginaryPart);
	LiczbaZespolona& WspolrzednaX() const;
	LiczbaZespolona& WspolrzednaY() const;
	LiczbaZespolona& WspolrzednaZ() const;

	void Wypisz() const;

	void Posprzataj();
};