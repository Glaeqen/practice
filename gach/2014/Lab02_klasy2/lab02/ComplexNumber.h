#pragma once

#include <iostream>

class LiczbaZespolona{
private:
	double _realPart;
	double _imaginaryPart;
public:
	LiczbaZespolona() : _realPart(0.0), _imaginaryPart(0.0){}
	LiczbaZespolona(double realPart, double _imaginaryPart) : _realPart(realPart), _imaginaryPart(_imaginaryPart){}
	
	void Wypisz() const{
		using std::cout;
		using std::endl;

		cout << "(" << _realPart << ", " << _imaginaryPart << ")";
	}
};