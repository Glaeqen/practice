#include <iostream>
#include "Vector.h"

Wektor *Wektor::UtworzConstWektor(const LiczbaZespolona& xCoord, const LiczbaZespolona& yCoord, const LiczbaZespolona& zCoord){
	Wektor *newVector = new Wektor(xCoord, yCoord, zCoord);
	return newVector;
}

Wektor& Wektor::UstawX(double realPart, double imaginaryPart){
	_xCoord = LiczbaZespolona(realPart, imaginaryPart);
	return *this;
}

Wektor& Wektor::UstawY(double realPart, double imaginaryPart){
	_yCoord = LiczbaZespolona(realPart, imaginaryPart);
	return *this;
}

Wektor& Wektor::UstawZ(double realPart, double imaginaryPart){
	_zCoord = LiczbaZespolona(realPart, imaginaryPart);
	return *this;
}

LiczbaZespolona& Wektor::WspolrzednaX() const{
	return _xCoord;
}

LiczbaZespolona& Wektor::WspolrzednaY() const{
	return _yCoord;
}

LiczbaZespolona& Wektor::WspolrzednaZ() const {
	return _zCoord;
}

void Wektor::Wypisz() const{
	using std::cout;
	using std::endl;

	cout << "[";
	WspolrzednaX().Wypisz();
	cout << ", ";
	WspolrzednaY().Wypisz();
	cout << ", ";
	WspolrzednaZ().Wypisz();
	cout << "]" << endl;
}

void Wektor::Posprzataj(){
	delete this;
}



