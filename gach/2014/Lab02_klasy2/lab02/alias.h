#pragma once

#define ComplexNumber LiczbaZespolona
#define Vector Wektor
#define print Wypisz
#define setX UstawX
#define setY UstawY
#define setZ UstawZ
#define xCoordinate WspolrzednaX
#define yCoordinate WspolrzednaY
#define zCoordinate WspolrzednaZ
#define initConstVector UtworzConstWektor
#define clean Posprzataj
