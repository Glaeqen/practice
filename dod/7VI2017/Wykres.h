#pragma once
#include <list>
#include <iostream>

#include "P3D.h"

template <typename T>
class Wykres : public std::list<T>{
public:
	void add(const T& object){
		std::list<T>::push_back(object);
	}
	void rem(unsigned index){
		typename std::list<T>::iterator it = std::list<T>::begin();
		std::advance(it, index);
		std::list<T>::erase(it);
	}
};

template <typename T>
std::ostream& operator<<(std::ostream& cout, const Wykres<T>& wykres){ return cout; }

template <>
std::ostream& operator<<(std::ostream& cout, const Wykres<P3D>& wykres){
	for(Wykres<P3D>::const_iterator it = wykres.begin(); it != wykres.end(); it++){
		cout << it->x << " " << it->y << " " << it->z << " " << std::endl;
	}
	return cout;
}

template <typename T1, typename T2>
std::ostream& operator<<(std::ostream& cout, const Wykres<std::pair<T1, T2> >& wykres){
	for(typename Wykres<std::pair<T1, T2> >::const_iterator it = wykres.begin(); it != wykres.end(); it++){
		cout << it->first << " " << it->second << std::endl;
	}
	return cout;
}

