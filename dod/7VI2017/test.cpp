/* W zadaniu nalezy uzyc kontenera standardowego zaby zaimplementowac obiekt Wykres.
  Uwaga: do wykresu czesto bedziemy dodawac i czesto usuwac punkty. 
         Nalezy uzyc odpowiedniego kontenera. (To bedzie element oceny.)
  Punkty sa w zasadzie obiektami dowolnego typu.
  Uwaga: Do przesuniecia iterator mozna uzyc funkcji std::advance ...

 */

#include <iostream>
#include <utility>

#include "Wykres.h"

#include "P3D.h"

typedef std::pair<int, int> P2DInt;

int main() {
  using namespace std;
  Wykres<P2DInt> pl2D;
  pl2D.add( make_pair(1, 2) );
  pl2D.add( make_pair(2, 5) );
  pl2D.add( make_pair(4, 2) );
  pl2D.add(  make_pair(3, 7) );

  cout << pl2D << endl;

  for ( Wykres<P2DInt>::const_iterator i = pl2D.begin(); i != pl2D.end(); ++i ) {
    std::cout << i->first << " ";
  }
  cout << endl;
  
  pl2D.rem( 3 );

  cout << pl2D << endl;

  Wykres<P3D> pl3D;
  pl3D.add(  P3D(0.1, 2, 12) );
  pl3D.add(  P3D(0.1, 2, 0.3) );
  pl3D.add(  P3D(0.1, 0.2, 0.3) );
  pl3D.add(  P3D(0.2, 6, 10) );
  pl3D.add(  P3D(0.2, 2.1, 0.4) );
  pl3D.add(  P3D(0.2, 0.2, 0.2) );
  cout << pl3D << endl;

  pl3D.rem( 0 );
  pl3D.rem( 3 );
  cout << pl3D << endl;
  for ( Wykres<P3D>::const_iterator i = pl3D.begin(); i != pl3D.end(); ++i ) {
    std::cout << i->y;
  }  
}
/* wynik
1 2
2 5
4 2
3 7

1 2 4 3 
1 2
2 5
4 2

0.1 2 12
0.1 2 0.3
0.1 0.2 0.3
0.2 6 10
0.2 2.1 0.4
0.2 0.2 0.2

0.1 2 0.3
0.1 0.2 0.3
0.2 6 10
0.2 0.2 0.2

20.260.2
 */
