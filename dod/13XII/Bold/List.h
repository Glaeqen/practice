#pragma once

class Node{
	char *_string;
	Node *_next;
public:
	Node(const char *string);

	void setNext(Node *next);

	char *data() const;
	Node *next() const;
	~Node();
};

class List{
	Node *_head;
	Node *_tail;
public:
	List();
	List& add(const char *string);
	void clear();

	Node *head() const;
	void dump() const;
	bool empty() const;
	~List();
};