#include <iostream>
#include <cstring>

#include "List.h"

using std::cout;

Node::Node(const char *string)
	:	_string(NULL),
		_next(NULL)
{
	int stringLength = strlen(string) + 1;
	_string = new char[stringLength];
	strcpy(_string, string);
}

void Node::setNext(Node *next){
	_next = next;
}

char *Node::data() const{
	return _string;
}

Node *Node::next() const{
	return _next;
}

Node::~Node(){
	delete[] _string;
	delete _next;
}

List::List()
	:	_head(NULL),
		_tail(NULL)
{}

List& List::add(const char *string){
	Node *newNode = new Node(string);
	if(_head == NULL){
		_head = _tail = newNode;
	}
	else{
		_tail->setNext(newNode);
		_tail = newNode;
	}
	return *this;
}

void List::clear(){
	this->~List();
}

Node *List::head() const{
	return _head;
}

void List::dump() const{
	if(_head == NULL) return;
	Node *tempNode = _head;
	for(; tempNode; tempNode = tempNode->next()){
		cout << tempNode->data() << " ";
	}
}

bool List::empty() const{
	return _head == NULL;
}

List::~List(){
	delete _head;
	_head = NULL;
	_tail = NULL;
}