#pragma once

class Student;

class Grade{
	int _grade;
	Student& _student;	
public:
	Grade(int grade, Student& student);
	int getGrade() const;
	void print() const;
};