#include <iostream>

#include "Grade.h"
#include "Student.h"

using std::cout;
using std::endl;

Grade::Grade(int grade, Student& student)
	:	_grade(grade),
		_student(student)

{
	_student.addGrade(this);
}

int Grade::getGrade() const{
	return _grade;
}

void Grade::print() const{
	cout << "Grade print value=" << _grade << " studentID=" << _student.getID() << endl;
}