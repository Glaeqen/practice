#include <iostream>

#include "Grade.h"
#include "Student.h"

using std::cout;
using std::endl;

void Student::increaseArraySize(){
	Grade **newGrades = new Grade*[_gradesVolume+1];
	for(int i=0; i<_gradesVolume; i++){
		newGrades[i] = _grades[i];
	}
	_gradesVolume++;

	delete[] _grades;
	_grades = newGrades;
}

void Student::printGrades() const{
	for(int i=0; i<_gradesVolume; i++){
		cout << _grades[i]->getGrade();
		if(i != _gradesVolume-1)	cout << " ";
	}
}

Student::Student()
	: 	_id(0),
		_gradesVolume(0),
		_grades(NULL)
{}

void Student::addGrade(Grade *grade){
	increaseArraySize();
	_grades[_gradesVolume-1] = grade;

}

void Student::setID(int id){
	_id = id;
}

int Student::getID() const{
	return _id;
}

void Student::print() const{
	cout << "Student print ID=" << _id;
	if(_grades != NULL){
		cout << " grades: ";
		printGrades();
	}
	cout << endl;
}

Student::~Student(){
	delete[] _grades;
}