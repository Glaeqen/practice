#pragma once

class Grade;

class Student{
	int _id;
	Grade **_grades;
	int _gradesVolume;

	void increaseArraySize();
	void printGrades() const;
public:
	Student();

	void addGrade(Grade *grade);
	void setID(int id);

	int getID() const;
	void print() const;

	~Student();
};