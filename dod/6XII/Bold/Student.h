#pragma once

class Students;

class Student{
private:
	char *_name1;
	char *_name2;
	char *_surname;
	int _year;
public:
	Student(const char *name1, const char *name2, const char *surname, int year);

	char *name1() const;
	char *name2() const;
	char *surname() const;
	int year() const;

	~Student();
};