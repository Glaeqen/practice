#include <iostream>

#include "Students.h"

void Students::copyData(Student **destArray, Student **sourceArray){
	for(int i=0; i<_studentArrayVolume; i++){
		destArray[i] = sourceArray[i];
	}
	destArray[_studentArrayVolume] = NULL;
}

Students::Students(){
	_studentArray = NULL;
	_studentArrayVolume = 0;
}

void Students::add(Student *student){
	if(_studentArray == NULL){
		_studentArrayVolume++;
		_studentArray = new Student*[_studentArrayVolume];
		*_studentArray = student;
		return;
	}
	Student **newStudentArray = new Student*[_studentArrayVolume+1];
	copyData(newStudentArray, _studentArray);
	delete[] _studentArray;
	newStudentArray[_studentArrayVolume] = student;
	_studentArrayVolume++;
	_studentArray = newStudentArray;
}

void Students::add(const Student& student){
	Student *newStudent = new Student(student.name1(), student.name2(), student.surname(), student.year());
	add(newStudent);
}

Student *Students::at(int index) const{
	return _studentArray[index];
}

void Students::printNames() const{
	using std::cout;
	using std::endl;
	for(int i=0; i<_studentArrayVolume; i++){
		cout << _studentArray[i]->name1() << " " << _studentArray[i]->name2() << endl;
	}
}

void Students::printSurnames() const{
	using std::cout;
	using std::endl;
	for(int i=0; i<_studentArrayVolume; i++){
		cout << _studentArray[i]->surname() << endl;
	}
}

void Students::printAll() const{
	using std::cout;
	using std::endl;
	for(int i=0; i<_studentArrayVolume; i++){
		cout << _studentArray[i]->name1() << " " << _studentArray[i]->name2() << " ";
		cout << _studentArray[i]->surname() << " " << _studentArray[i]->year() << endl;
	}
}

void Students::clear(){
	for(int i=0; i<_studentArrayVolume; i++){
		delete _studentArray[i];
	}
	delete[] _studentArray;

	_studentArray = NULL;
	_studentArrayVolume = 0;
}