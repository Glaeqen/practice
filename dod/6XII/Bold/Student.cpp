#include <cstring>
#include "Student.h"

Student::Student(const char *name1, const char *name2, const char *surname, int year){
	int name1Length = strlen(name1) + 1;
	int name2Length = strlen(name2) + 1;
	int surnameLength = strlen(surname) + 1;

	_name1 = new char[name1Length];
	_name2 = new char[name2Length];
	_surname = new char[surnameLength];

	strcpy(_name1, name1);
	strcpy(_name2, name2);
	strcpy(_surname, surname);

	_year = year;
}

char *Student::name1() const{
	return _name1;
}

char *Student::name2() const{
	return _name2;
}

char *Student::surname() const{
	return _surname;
}

int Student::year() const{
	return _year;
}

Student::~Student(){
	delete[] _name1;
	delete[] _name2;
	delete[] _surname;
}