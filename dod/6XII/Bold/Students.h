#pragma once

#include "Student.h"

class Students{
private:
	Student **_studentArray;
	int _studentArrayVolume;

	void copyData(Student **destArray, Student **sourceArray);
public:
	Students();

	void add(Student *student);
	void add(const Student& student);

	Student *at(int index) const;
	void printNames() const;
	void printSurnames() const;
	void printAll() const;

	void clear();
};