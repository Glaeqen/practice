#pragma once

#include "Student.h"

class StudentsList{
private:
	char *_listName;
	Student *_head;
	Student *_tail;
	StudentsList();
public:
	StudentsList(const char *listName);

	void AddStudent(Student *student);
	void AddStudent(const char *name, const char *surname);

	Student *getHead() const;
	Student *getTail() const;
	void PrintInfo() const;
	void PrintAll() const;

	~StudentsList();
};