#include <iostream>
#include <cstring>

#include "StudentsList.h"

StudentsList::StudentsList(const char *listName){
	int listNameLength = strlen(listName) + 1;
	_listName = new char[listNameLength];

	strcpy(_listName, listName);

	_head = NULL;
	_tail = NULL;
}

void StudentsList::AddStudent(Student *student){
	student->setParentStudentsList(this);
	if(_head == NULL){
		_head = _tail = student;
		return;
	}

	_tail->setNextStudent(student);
	_tail = student;
}

void StudentsList::AddStudent(const char *name, const char *surname){
	Student *newStudent = new Student(name, surname);
	AddStudent(newStudent);
}

Student *StudentsList::getHead() const{
	return _head;
}
Student *StudentsList::getTail() const{
	return _tail;
}

void StudentsList::PrintInfo() const{
	using std::cout;
	using std::endl;
	cout << "Students List: ";
	cout << _listName << " ";
	cout << "(address: ";
	cout << this << ")" << endl;

}

void StudentsList::PrintAll() const{
	std::cout << _listName << std::endl;
	Student *tempStudent = _head;
	for(; tempStudent != NULL; tempStudent = tempStudent->getNextStudent()){
		tempStudent->Print();
	}
}

StudentsList::~StudentsList(){
	delete[] _listName;
	delete _head;
}
