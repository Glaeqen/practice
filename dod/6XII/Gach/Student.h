#pragma once

class StudentsList;

class Student{
private:
	char *_name;
	char *_surname;
	int _id;

	Student *_next;
	StudentsList *_parentStudentsList;
	Student();
public:
	static int studentsAmount;
	Student(const char *name, const char *surname);
	void setNextStudent(Student *next);
	void setParentStudentsList(StudentsList *parentStudentsList);

	Student *getNextStudent() const;
	StudentsList *getParentStudentsList() const;
	void Print() const;
	StudentsList *ParentList() const;

	~Student();
};