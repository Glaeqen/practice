#include <iostream>
#include <cstring>
#include "Student.h"

int Student::studentsAmount = 0;

Student::Student(const char *name, const char *surname){
	int nameLength = strlen(name) + 1;
	int surnameLength = strlen(surname) + 1;
	_name = new char[nameLength];
	_surname = new char[surnameLength];

	Student::studentsAmount++;
	_id = Student::studentsAmount;

	strcpy(_name, name);
	strcpy(_surname, surname);

	_next = NULL;
	_parentStudentsList = NULL;
}

void Student::setNextStudent(Student *next){
	_next = next;
}

void Student::setParentStudentsList(StudentsList *parentStudentsList){
	_parentStudentsList = parentStudentsList;
}

Student *Student::getNextStudent() const{
	return _next;
}

StudentsList *Student::getParentStudentsList() const{
	return _parentStudentsList;
}

void Student::Print() const{
	using std::cout;
	using std::endl;

	cout << "Student " << _id << ": ";
	cout << _name << ", " << _surname << endl;
}

StudentsList *Student::ParentList() const{
	return _parentStudentsList;
}

Student::~Student(){
	delete[] _name;
	delete[] _surname;
	delete _next;
}