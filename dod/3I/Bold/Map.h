#include <iostream>

class map_type_name{
	map_value_type *value_array_;
	map_key_type *key_array_;
	int volume_;

	void increase_array_size();
public:
	map_type_name();

	void insert(map_key_type key, map_value_type value);
	map_value_type& at(map_key_type key);
	
	map_value_type at(map_key_type key) const;
	unsigned int contains(map_key_type key) const;
	void print(const char *tag) const;

	~map_type_name();
};

void map_type_name::increase_array_size(){
	map_value_type *new_value_array = new map_value_type[volume_+1];
	map_key_type *new_key_array = new map_key_type[volume_+1];

	for(int i=0; i<volume_; i++){
		new_value_array[i] = value_array_[i];
		new_key_array[i] = key_array_[i];
	}

	delete[] value_array_;
	delete[] key_array_;

	value_array_ = new_value_array;
	key_array_ = new_key_array;
	volume_++;
}

map_type_name::map_type_name():
value_array_(NULL),
key_array_(NULL),
volume_(0)
{}

void map_type_name::insert(map_key_type key, map_value_type value){
	at(key) = value;
}

map_value_type& map_type_name::at(map_key_type key){
	bool exists = false;
	int exists_index;

	for(int i=0; i<volume_; i++){
		if(key_array_[i] == key){
			exists = true;
			exists_index = i;
			break;
		}
	}

	if(exists){
		return value_array_[exists_index];
	}
	else{
		increase_array_size();
		key_array_[volume_-1] = key;
		return value_array_[volume_-1];
	}
}


map_value_type map_type_name::at(map_key_type key) const{
	bool exists = false;
	int exists_index;
	for(int i=0; i<volume_; i++){
		if(key_array_[i] == key){
			exists = true;
			exists_index = i;
			break;
		}
	}

	if(exists){
		return value_array_[exists_index];
	}
	else{
		return 0;
	}
}

unsigned int map_type_name::contains(map_key_type key) const{
	unsigned int contains = 0;
	for(int i=0; i<volume_; i++){
		if(key_array_[i] == key){
			contains = true;
			break;
		}
	}
	return contains;
}

void map_type_name::print(const char *tag) const{
	using std::cout;
	using std::endl;

	for(int i=0; i<volume_; i++){
		cout << tag << " k: " << key_array_[i] << " v: " << value_array_[i] << endl;
	}
}

map_type_name::~map_type_name(){
	delete[] value_array_;
	delete[] key_array_;

	value_array_ = NULL;
	key_array_ = NULL;
	volume_ = 0;
}

