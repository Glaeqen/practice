#include "DoUndo.h"

DoUndoAction::~DoUndoAction(){}

KeepInt::KeepInt(int& value):rollbackCopy_(value), value_(value){}

void KeepInt::dodo(){}

void KeepInt::undoOk(){}

void KeepInt::undoFail(){
	value_ = rollbackCopy_;
}

bool DoUndo::isOk_ = false;

void DoUndo::allok(){
	isOk_ = true;
}

DoUndo::DoUndo(DoUndoAction *action):action_(action){
	isOk_ = false;
	action_->dodo();
}

DoUndo::~DoUndo(){
	if(isOk_){
		action_->undoOk();
	}
	else{
		action_->undoFail();
	}

	if(action_) delete action_;
	action_ = 0;
}