#pragma once

class DoUndoAction {
public:
	virtual void dodo() = 0;
  	virtual void undoOk() = 0;
  	virtual void undoFail() = 0;

  	virtual ~DoUndoAction();
};

class KeepInt : public DoUndoAction{
	int rollbackCopy_;
	int& value_;
public:
	KeepInt(int& value);
	void dodo();
  	void undoOk();
  	void undoFail();
};

class DoUndo {
	DoUndoAction *action_;
	static bool isOk_;
public:
	static void allok();
	DoUndo(DoUndoAction *action);
	~DoUndo();
};