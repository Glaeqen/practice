#pragma once
#include <iostream>

class Pomieszczenie{
	const char *name_;
public:
	Pomieszczenie(const char *name): name_(name){
		std::cout << "Tworze pomieszcznie: " << name_ << std::endl;
	}
	void zetrzyj_kurze() const{
		std::cout << "Scieram kurze w pomieszczeniu: " << name_ << std::endl;
 	}
	void umyj_podloge() const{
		std::cout << "Myje podloge w pomieszczeniu: " << name_ << std::endl;
	}
	void odkurz() const{
		std::cout << "Odkurzam pomieszczenie: " << name_ << std::endl;
	}
	template <typename T>
	void wstaw(T value){
		std::cout << "Wstawiam " << value << " do pomieszczenia: " << name_ << std::endl;
	}

	template <void (Pomieszczenie::*function)(void) const>
	friend void Lokaj(const Pomieszczenie& pomieszczenie); // bo friendy best design pattern ever. 
};

template <void (Pomieszczenie::*function)(void) const>
void Lokaj(Pomieszczenie& pomieszczenie){
	(pomieszczenie.*function)();
}

template <void (Pomieszczenie::*function)(void) const>
void Lokaj(const Pomieszczenie& pomieszczenie){
	std::cout << "Pomieszczenie " << pomieszczenie.name_ << " jest zamknięte." << std::endl;
}

template <int>
void Lokaj(const Pomieszczenie& pomieszczenie){
	std::cout << "Nic nie robie." << std::endl;
}