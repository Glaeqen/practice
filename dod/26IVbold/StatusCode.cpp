#include "StatusCode.h"
#include <iostream>
#include <exception>

void UncheckedStausHandler::handle(){
	try{
		throw;
	}
	catch(UncheckedStatusCode& e){
		std::cout << "..Handling UncheckedStatusCode " << std::endl;
	}
	catch(std::exception& e){
		std::cout << "..Handling std::exception " << e.what() << std::endl;
	}
}

StatusCode StatusCode::ALLOK(){
	return StatusCode();
}

StatusCode StatusCode::ERROR(const std::string& errorMessage){
	return StatusCode(errorMessage);
}

StatusCode::StatusCode(const std::string& errorMessage):wasChecked_(false),isValidChecker_(true){
	if(errorMessage.size()){
		isOk_ = false;
		errorMessage_ = errorMessage;
	}
	else{
		isOk_ = true;
	}
}

StatusCode::StatusCode(const StatusCode& statusCode):wasChecked_(false),isValidChecker_(true){
	statusCode.isValidChecker_ = false;

	isOk_ = statusCode.isOk_;
	errorMessage_ = statusCode.errorMessage_;
}

bool StatusCode::isok(){
	wasChecked_ = true;
	return isOk_;
}

std::string StatusCode::info() const{
	return errorMessage_;
}

StatusCode::~StatusCode(){
	if(isValidChecker_){
		if(!wasChecked_) throw UncheckedStatusCode(); //CANCUR CANCUR
	}
}
