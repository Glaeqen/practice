#pragma once
#include <string>

class UncheckedStatusCode {};

class UncheckedStausHandler {
public:
	static void handle();
};


class StatusCode{
	std::string errorMessage_;
	bool isOk_;
	bool wasChecked_;
	mutable bool isValidChecker_;
public:
	static StatusCode ALLOK();
	static StatusCode ERROR(const std::string& errorMessage);

	StatusCode(const std::string& errorMessage = std::string());
	StatusCode(const StatusCode& statusCode);

	bool isok();
	std::string info() const;

	~StatusCode();
};