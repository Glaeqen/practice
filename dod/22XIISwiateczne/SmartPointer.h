#pragma once
#define SmartPointerValueType TestClassA

class SmartPointerValueType;

class SmartPointer {
	SmartPointerValueType *smartPointerValueType_;
	int *aliveReferences_;

	void overwriteWith(const SmartPointer& smartPointer);
public:
	SmartPointer(SmartPointerValueType *smartPointerValueType);
	SmartPointer(const SmartPointer& smartPointer);

	SmartPointer& operator = (const SmartPointer& right);

	SmartPointerValueType *data() const;
	int *getAliveReferences() const;

	~SmartPointer();
};