
#ifndef TestClassA_h
#define TestClassA_h

class TestClassA {
public:
  TestClassA(const char* str);
  ~TestClassA();
  const char* name() const { return m_name; }
private:
  char* m_name;
};


#endif // TestClass_h
