#include <string.h>
#include <iostream>
using namespace std;
#include "TestClassA.h"



TestClassA::TestClassA(const char* str) {
  m_name = new char[strlen(str)+1];
  strcpy(m_name, str);
  cout << ".. Konstruuje TestClassA " << m_name << endl;
  
}

TestClassA::~TestClassA() {
  cout << ".. Usuwam  TestClassA " << m_name << endl;
  delete[] m_name;
}