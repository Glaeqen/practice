#include "TestClassA.h"
#include "SmartPointer.h"

#define NULL 0

void SmartPointer::overwriteWith(const SmartPointer& smartPointer){
	aliveReferences_ = smartPointer.getAliveReferences();
	smartPointerValueType_ = smartPointer.data();

	(*aliveReferences_)++;
}

SmartPointer::SmartPointer(SmartPointerValueType *smartPointerValueType){
	aliveReferences_ = new int;

	*aliveReferences_ = 1;
	smartPointerValueType_ = smartPointerValueType;
}

SmartPointer::SmartPointer(const SmartPointer& smartPointer){
	overwriteWith(smartPointer);
}

SmartPointer& SmartPointer::operator = (const SmartPointer& right){
	//Checks if we don't do sth like "x = x;"
	if(this != &right){
		//Checks if SmartPointers are wrapping different pointers.
		if(smartPointerValueType_ != right.data()){
			//Checks if overwritten SmartPointer is the last one.
			//If it is, destructor is called first.
			if(*aliveReferences_ == 1){
				this->~SmartPointer();
			}
			overwriteWith(right);
		}
		//If both SmartPointers wrap the same pointer, it means
		//that SmartPointer is duplicated, so:
		else	(*aliveReferences_)++;
	}
	return *this;
}

SmartPointerValueType *SmartPointer::data() const{
	return smartPointerValueType_;
}

int *SmartPointer::getAliveReferences() const{
	return aliveReferences_;
}

SmartPointer::~SmartPointer(){
	(*aliveReferences_)--;

	//If there's no alive references -> destroy.
	if(!*aliveReferences_){
		delete smartPointerValueType_;
		delete aliveReferences_;
		
		smartPointerValueType_ = NULL;
		aliveReferences_ = NULL;
	}
}




