#pragma once

class StackNode{
	char *_data;
	StackNode *_below;
public:
	StackNode(const char *data, StackNode *below);
	StackNode(const StackNode& stackNode);

	void setBelow(StackNode *below);

	const char *getData() const;
	StackNode *getBelow() const;

	~StackNode();
};