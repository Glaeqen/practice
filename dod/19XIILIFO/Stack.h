#pragma once

class StackNode;

class Stack {
	StackNode *_top;
	unsigned int _stackSize;
public:
	Stack();
	Stack(const Stack& stack);

	void push(const char *data);
	void pop(char *outputBuffer);

	bool isEmpty() const;

	~Stack();
};