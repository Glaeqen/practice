#include <cstring>

#include "StackNode.h"

StackNode::StackNode(const char *data, StackNode *below){
	int dataLength = strlen(data) + 1;
	_data = new char[dataLength];

	strcpy(_data, data);
	_below = below;
}

StackNode::StackNode(const StackNode& stackNode){
	const char *data = stackNode.getData();
	int dataLength = strlen(data);

	_data = new char[dataLength];

	strcpy(_data, data);
	_below = stackNode.getBelow();
}

void StackNode::setBelow(StackNode *below){
	_below = below;
}

const char *StackNode::getData() const{
	return _data;
}

StackNode *StackNode::getBelow() const{
	return _below;
}

StackNode::~StackNode(){
	delete[] _data;
	_data = NULL;
	_below = NULL;
}
