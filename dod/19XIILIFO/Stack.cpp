#include <cstring>

#include "StackNode.h"
#include "Stack.h"

Stack::Stack():
	_top(NULL),
	_stackSize(0)
{}

Stack::Stack(const Stack& stack){
	//TODO
}

void Stack::push(const char *data){
	StackNode *newStackNode = new StackNode(data, _top);
	_top = newStackNode;
	_stackSize++;
}

void Stack::pop(char *outputBuffer){
	if(!_stackSize){
		strcpy(outputBuffer, "");
		return;
	}

	strcpy(outputBuffer, _top->getData());
	StackNode *stackNodeToFree = _top;
	_top = _top->getBelow();
	delete stackNodeToFree;

	_stackSize--;
}

bool Stack::isEmpty() const{
	return !_stackSize;
}

Stack::~Stack(){
	delete _top;
	_top = NULL;
	_stackSize = 0;
}
