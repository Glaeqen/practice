#include <iostream>

#include "Stack.h"
#include "StackNode.h"

int main(){
	Stack s;
	s.push("to jest pierwsze");
	s.push("to je drugie.");
	s.push("no elo. czecie.");
	s.push("wololo");

	char *buffer = new char[100];
	s.pop(buffer);
	while(!s.isEmpty()){
		s.pop(buffer);
		std::cout << buffer << std::endl;
	}
	delete[] buffer;
	return 0;
}

