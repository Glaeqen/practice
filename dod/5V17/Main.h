#pragma once

class TestObject{
	int ID_;
public:
	TestObject(): ID_(0){};
	TestObject(int ID): ID_(ID){};
	void Print() const{
		std::cout << "Object with ID=" << ID_ << std::endl;
	};
	void Set(int ID){
		ID_ = ID;
	};
};

class Vector{
	static void *vector_;
	static int size_;
public:
	template <typename T>
	static void Initialise(int size, T initialiser = T(0));

	template <typename T>
	static T& At(int index);

	static int Size();

	template <typename T>
	static void Reset();
};

void *Vector::vector_ = NULL;

int Vector::size_ = 0;

template <typename T>
void Vector::Initialise(int size, T initialiser){
	T *newVector = new T[size];
	size_ = size;
	for(int i=0; i<size_; i++){
		newVector[i] = initialiser;
	}

	vector_ = newVector;
}

template <typename T>
T& Vector::At(int index){

	return ((T*)vector_)[index];
}

int Vector::Size(){
	return size_;
}

template <typename T>
void Vector::Reset(){
	delete[] (T*)vector_;
	vector_ = NULL;
	size_ = 0;
}