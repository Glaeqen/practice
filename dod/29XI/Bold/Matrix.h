#pragma once

#include "Vector.h"

class Matrix{
private:
	Vector* vectors[3];
	Matrix();
public:
	Matrix(Vector& v1, Vector& v2, Vector& v3);

	Matrix& set(int row, int column, double value);

	Vector& extractRow(int row);
	Vector extractColumn(int column); 
};