#pragma once

class Vector{
private:
	double coordinates[3];

public:
	Vector();
	Vector(double x, double y, double z);

	void setCoord(int coordinate, double value);
	double at(int coordinate)const;
};