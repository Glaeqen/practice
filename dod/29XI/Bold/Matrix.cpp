#include "Matrix.h"

Matrix::Matrix(Vector& v1, Vector& v2, Vector& v3){
	vectors[0] = &v1;
	vectors[1] = &v2;
	vectors[2] = &v3;
}

Matrix& Matrix::set(int row, int column, double value){
	vectors[row]->setCoord(column, value);
	return *this;
}

Vector& Matrix::extractRow(int row){
	return *vectors[row];
}

Vector Matrix::extractColumn(int column){
	double values[3];
	for(int i=0; i<3; i++){
		values[i] = vectors[i]->at(column); 
	}
	return Vector (values[0], values[1], values[2]);
}
