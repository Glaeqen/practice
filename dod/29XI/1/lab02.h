#pragma once

class Wektor{
private:
	int dimensions;
	float *coordinates;
public:
	Wektor(const int dimensions);
	Wektor(const int dimensions, float *coordinates);

	Wektor& Ustaw(const int dimension, const float coordinate);
	
	float& Wspolrzedna(const int coordinate) const;
	float Iloczyn(const Wektor& wektor) const;
	void CzyConst(Wektor& wektor) const;
	void CzyConst(const Wektor& wektor) const;
	void Wypisz() const;

	~Wektor();
};

	