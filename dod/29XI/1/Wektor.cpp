#include <iostream>
#include "lab02.h"

Wektor::Wektor(const int dimensions){
	this->dimensions = dimensions;
	coordinates = new float[dimensions];
	for(int i=0; i<dimensions; i++){
		coordinates[i] = 0;
	}
}

Wektor::Wektor(const int dimensions, float *coordinates){
	this->dimensions = dimensions;
	this->coordinates = coordinates;
}

Wektor& Wektor::Ustaw(const int dimension, const float coordinate){
	coordinates[dimension] = coordinate;
	return *this;
}

float& Wektor::Wspolrzedna(const int coordinate)const{
	return coordinates[coordinate];
}

float Wektor::Iloczyn(const Wektor& wektor) const{
	if(wektor.dimensions != dimensions){
		std::cout << "Vectors has got different dimensions, returning.." << std::endl;
		return 0;
	}

	float sum = 0;

	for(int i=0; i<wektor.dimensions; i++){
		sum+=wektor.coordinates[i]*coordinates[i];
	}

	return sum;
}

void Wektor::CzyConst(Wektor& wektor) const{
	std::cout << "nie" << std::endl;
}

void Wektor::CzyConst(const Wektor& wektor) const{
	std::cout << "tak" << std::endl;
}

void Wektor::Wypisz() const{
	std::cout << "(";
	int i;
	for(i=0; i<dimensions-1; i++){
		std::cout << coordinates[i] << ", ";
	}
	std::cout << coordinates[i] << ")" << std::endl;
}

Wektor::~Wektor(){
	delete[] coordinates;
}