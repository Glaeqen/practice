#include <iostream>
#include <cmath>

#include "MapDistance.h"

MapDistance::MapDistance(double distanceLat, double distanceLon){
	_distanceLat = distanceLat;
	_distanceLon = distanceLon;
	_distance = sqrt(pow(distanceLat, 2)+pow(distanceLon, 2));
}

double MapDistance::getDistance()const{
	return _distance;
}

void MapDistance::print()const{
	std::cout << "Distance: (";
	std::cout << fabs(_distanceLat) << ", ";
	std::cout << fabs(_distanceLon) << ")" << std::endl;
}