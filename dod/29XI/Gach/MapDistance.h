#pragma once

class MapDistance{
private:
	double _distanceLat;
	double _distanceLon;
	double _distance;
	MapDistance();
public:
	MapDistance(double distanceLat, double distanceLon);
	
	double getDistance()const;

	void print()const;
};
