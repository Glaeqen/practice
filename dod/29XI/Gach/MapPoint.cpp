#include <iostream>
#include <cmath>

#include "MapPoint.h"
#include "MapDistance.h"

unsigned int MapPoint::next_free_id = 1;

double MapPoint::get_latitude()const{
	return _latitude;
}

double MapPoint::get_longitude()const{
	return _longitude;
}

void MapPoint::set_latitude(double latitude){
	_latitude = latitude;
}

void MapPoint::set_longitude(double longitude){
	_longitude = longitude;
}

void MapPoint::set_coordinates(double latitude, double longitude){
	set_latitude(latitude);
	set_longitude(longitude);
}

const MapPoint& MapPoint::closest(const MapPoint& mapPoint1, const MapPoint& mapPoint2)const{
	MapDistance mapDistance1 = distance(mapPoint1);
	MapDistance mapDistance2 = distance(mapPoint2);
	
	if(mapDistance1.getDistance() < mapDistance2.getDistance())	return mapPoint1;
	else return mapPoint2;
}

MapDistance MapPoint::distance(const MapPoint& mapPoint)const{
	MapDistance mapDistance(mapPoint.get_latitude()-_latitude, mapPoint.get_longitude()-_longitude);
	return mapDistance;
}

void MapPoint::assign_id(){
	_personal_id = next_free_id;
	next_free_id++;
}

void MapPoint::print()const{
	std::cout << "Point with ID=" << _personal_id;
	std::cout << " : (";
	std::cout << _latitude << ", " << _longitude;
	std::cout << ")" << std::endl;
}