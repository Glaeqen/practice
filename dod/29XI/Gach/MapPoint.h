#pragma once

#include "MapDistance.h"

class MapPoint{
private:
	unsigned int _personal_id;
	double _latitude;
	double _longitude;
public:
	static unsigned int next_free_id;

	double get_latitude()const;
	double get_longitude()const;

	void set_latitude(double latitude);
	void set_longitude(double longitude);
	void set_coordinates(double latitude, double longitude);

	const MapPoint& closest(const MapPoint& mapPoint1, const MapPoint& mapPoint2)const;
	MapDistance distance(const MapPoint& mapPoint)const;

	void assign_id();
	void print()const;
};