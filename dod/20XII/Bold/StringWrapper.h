#pragma once

class StringWrapper {
	char *_string;
	unsigned int _stringLength;

	void reallocMemory(int newStringLength);
public:
	StringWrapper(const char *string);
	StringWrapper(const StringWrapper& stringWrapper);

	static bool eq(const StringWrapper& string1, const StringWrapper& string2);
	static bool eqIcase(const StringWrapper& string1, const StringWrapper& string2);

	StringWrapper& append(const char *string);
	StringWrapper& append(const StringWrapper& string);

	const char *data() const;
	unsigned int getStringLength() const;
	StringWrapper substring(unsigned int start, unsigned int end) const;

	~StringWrapper();
};

void print(const StringWrapper& string);
void print(const char* string);