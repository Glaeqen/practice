#include <cstring>
#include <cctype>
#include <iostream>

#include "StringWrapper.h"

using std::cout;
using std::endl;

void StringWrapper::reallocMemory(int newStringLength){
	char *newString = new char[newStringLength];
	for(int i=0; i<_stringLength; i++){
		newString[i] = _string[i];
	}
	_stringLength = newStringLength;
	delete[] _string;
	_string = newString;
}

StringWrapper::StringWrapper(const char *string){
	unsigned int stringLength = strlen(string) + 1;
	_string = new char[stringLength];

	strcpy(_string, string);
	_stringLength = stringLength;
}

StringWrapper::StringWrapper(const StringWrapper& stringWrapper){
	_stringLength = stringWrapper.getStringLength();
	_string = new char[_stringLength];

	strcpy(_string, stringWrapper.data());
}

bool StringWrapper::eq(const StringWrapper& string1, const StringWrapper& string2){
	unsigned int string1Length = string1.getStringLength();
	unsigned int string2Length = string2.getStringLength();
	if(string1Length != string2Length)	return false;

	const char *string1Data = string1.data();
	const char *string2Data = string2.data(); 
	for(int i=0; i<string1Length; i++){
		if(string1Data[i] != string2Data[i])	return false;
	}
	return true;
}

bool StringWrapper::eqIcase(const StringWrapper& string1, const StringWrapper& string2){
	unsigned int string1Length = string1.getStringLength();
	unsigned int string2Length = string2.getStringLength();
	if(string1Length != string2Length)	return false;

	const char *string1Data = string1.data();
	const char *string2Data = string2.data(); 
	for(int i=0; i<string1Length; i++){
		if(tolower(string1Data[i]) != tolower(string2Data[i]))	return false;
	}
	return true;
}

StringWrapper& StringWrapper::append(const char *string){
	int newStringLength = strlen(string) + _stringLength;

	reallocMemory(newStringLength);

	for(int _stringIter=strlen(_string), stringIter=0; _stringIter<newStringLength; _stringIter++, stringIter++){
		_string[_stringIter] = string[stringIter];
	}

	return *this;
}

StringWrapper& StringWrapper::append(const StringWrapper& string){
	int newStringLength = string.getStringLength() + _stringLength - 1;
	const char *stringData = string.data();

	reallocMemory(newStringLength);

	for(int _stringIter=strlen(_string), stringDataIter=0; _stringIter<newStringLength; _stringIter++, stringDataIter++){
		_string[_stringIter] = stringData[stringDataIter];
	}

	return *this;
}

const char *StringWrapper::data() const{
	return _string;
}

unsigned int StringWrapper::getStringLength() const{
	return _stringLength;
}

StringWrapper StringWrapper::substring(unsigned int start, unsigned int end) const{
	char *tempString = new char[end-start+1];
	for(int _stringIter=start, tempStringIter=0; tempStringIter<end-start; _stringIter++, tempStringIter++){
		tempString[tempStringIter] = _string[_stringIter];
	}
	StringWrapper stringWrapper(tempString);
	delete[] tempString;
	return stringWrapper;
}

StringWrapper::~StringWrapper(){
	delete[] _string;
	_string = NULL;
	_stringLength = 0;
}

void print(const StringWrapper& string){
	cout << string.data() << endl;
}

void print(const char *string){
	cout << string << endl;
}