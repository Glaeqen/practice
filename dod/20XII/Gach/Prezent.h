#pragma once

class Prezent {
	const float _wartoscPrezentu;

	static unsigned int iloscPrezentow;
public:
	Prezent(float wartoscPrezentu);
	Prezent(const Prezent& prezent);

	void Wypisz() const;
	void NapiszIleJestPrezentow() const;
	float GetWartoscPrezentu() const;
	
	~Prezent();
};