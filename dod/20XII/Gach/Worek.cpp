#include <iostream>
#include "Worek.h"

using std::cout;
using std::endl;

PrezentWWorku::PrezentWWorku(const Prezent& prezent, PrezentWWorku *below):
	_prezent(prezent),
	_below(below)
{}

PrezentWWorku::PrezentWWorku(const PrezentWWorku& prezentWWorku):
	_prezent(prezentWWorku.GetPrezent()),
	_below(prezentWWorku.GetBelow())
{}

void PrezentWWorku::SetBelow(PrezentWWorku *below){
	_below = below;
}

void PrezentWWorku::Wypisz(){
	_prezent.Wypisz();
}

PrezentWWorku *PrezentWWorku::GetBelow() const{
	return _below;
}

const Prezent& PrezentWWorku::GetPrezent() const{
	return _prezent;
}


Worek::Worek():
_top(NULL)
{}

void Worek::Wloz(const Prezent& prezent){
	PrezentWWorku *newPrezentWWorku = new PrezentWWorku(prezent, _top);
	_top = newPrezentWWorku;
}

PrezentWWorku Worek::Wyciagnij(){
	if(!_top){
		cout << "Niestety, prezenty juz sie skonczyly." << endl;
		return PrezentWWorku(Prezent(0.0), NULL);
	}

	PrezentWWorku returnedPrezentWWorku(*_top);

	PrezentWWorku *topToDelete = _top;
	_top = _top->GetBelow();
	delete topToDelete;

	return returnedPrezentWWorku;
}

Worek::~Worek(){
	if(_top)	delete _top;
}