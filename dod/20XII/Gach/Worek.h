#pragma once

#include "Prezent.h"

class PrezentWWorku {
	Prezent _prezent;
	PrezentWWorku *_below;
public:
	PrezentWWorku(const Prezent& prezent, PrezentWWorku *below);
	PrezentWWorku(const PrezentWWorku& prezentWWorku);

	void SetBelow(PrezentWWorku *below);
	void Wypisz();

	PrezentWWorku *GetBelow() const;
	const Prezent& GetPrezent() const;
};

class Worek {
	PrezentWWorku *_top;
public:
	Worek();

	void Wloz(const Prezent& prezent);

	PrezentWWorku Wyciagnij();

	~Worek();
};