#include <iostream>
#include "Prezent.h"

using std::cout;
using std::endl;

unsigned int Prezent::iloscPrezentow = 0;

Prezent::Prezent(float wartoscPrezentu):
	_wartoscPrezentu(wartoscPrezentu)
{
	iloscPrezentow++;
}

Prezent::Prezent(const Prezent& prezent):
	_wartoscPrezentu(prezent.GetWartoscPrezentu())
{
	iloscPrezentow++;
}

void Prezent::Wypisz() const{
	cout << "Ten prezent kosztuje: " << _wartoscPrezentu << endl;
}

void Prezent::NapiszIleJestPrezentow() const{
	cout << "Aktualna liczba prezentow: " << iloscPrezentow << endl;
}

float Prezent::GetWartoscPrezentu() const{
	return _wartoscPrezentu;
}

Prezent::~Prezent(){
	iloscPrezentow--;
}