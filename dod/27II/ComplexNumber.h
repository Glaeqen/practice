#pragma once
#include <iostream>

using std::ostream;
using std::cout;
using std::endl;

enum Indicator{
	_REAL_,
	_IMAGINARY_
};

class ComplexNumber{
	int real_;
	int imaginary_;
public:
	ComplexNumber(int real, int imaginary = 0);
	ComplexNumber(const ComplexNumber& complexNumber);

	static const ComplexNumber i;
	static const Indicator REAL;
	static const Indicator IMAGINARY;

	ComplexNumber& operator=(const ComplexNumber& right);
	void operator*=(const ComplexNumber& right);
	void operator+=(const ComplexNumber& right);
	ComplexNumber& operator++();
	ComplexNumber operator++(int);

	int& operator[](Indicator indicator);

	ComplexNumber operator+(const ComplexNumber& right) const;
	ComplexNumber operator-(const ComplexNumber& right) const;
	ComplexNumber operator*(const ComplexNumber& right) const;
	bool operator>(const ComplexNumber& right) const;


	friend ostream& operator<<(ostream& cout, const ComplexNumber& complexNumber);
	friend ComplexNumber operator+(int left, const ComplexNumber& right);
	friend ComplexNumber operator-(int left, const ComplexNumber& right);
};

ostream& operator<<(ostream& cout, const ComplexNumber& complexNumber);
ComplexNumber operator+(int left, const ComplexNumber& right);
ComplexNumber operator-(int left, const ComplexNumber& right);