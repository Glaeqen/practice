#include "ComplexNumber.h"

const ComplexNumber ComplexNumber::i = ComplexNumber(0, 1);
const Indicator ComplexNumber::REAL = _REAL_;
const Indicator ComplexNumber::IMAGINARY = _IMAGINARY_;

ComplexNumber::ComplexNumber(int real, int imaginary):real_(real), imaginary_(imaginary){}

ComplexNumber::ComplexNumber(const ComplexNumber& complexNumber):real_(complexNumber.real_), imaginary_(complexNumber.imaginary_){}

ComplexNumber& ComplexNumber::operator=(const ComplexNumber& right){
	if(this == &right)	return *this;
	real_ = right.real_;
	imaginary_ = right.imaginary_;
	return *this;
}

void ComplexNumber::operator*=(const ComplexNumber& right){
	real_ = real_ * right.real_ - imaginary_ * right.imaginary_;
	imaginary_ = imaginary_ * right.real_  + real_ * right.imaginary_;
}

void ComplexNumber::operator+=(const ComplexNumber& right){
	real_ += right.real_;
	imaginary_ += right.imaginary_;
}

ComplexNumber& ComplexNumber::operator++(){
	--real_;
	return *this;
}

ComplexNumber ComplexNumber::operator++(int){
	ComplexNumber result(*this);
	--real_;
	return result;
}

int& ComplexNumber::operator[](Indicator indicator){
	switch (indicator){
		case _REAL_:	return real_;
		case _IMAGINARY_:	return imaginary_;
	}
}

ComplexNumber ComplexNumber::operator+(const ComplexNumber& right) const{
	return ComplexNumber(real_ + right.real_, imaginary_ + right.imaginary_);
}

ComplexNumber ComplexNumber::operator-(const ComplexNumber& right) const{
	return ComplexNumber(real_ - right.real_, imaginary_ - right.imaginary_);
}

ComplexNumber ComplexNumber::operator*(const ComplexNumber& right) const{
	return ComplexNumber(real_ * right.real_ - imaginary_ * right.imaginary_, imaginary_ * right.real_  + real_ * right.imaginary_);
}

bool ComplexNumber::operator>(const ComplexNumber& right) const{
	int thisVolume = real_*real_ + imaginary_*imaginary_;
	int rightVolume = right.real_*right.real_ + right.imaginary_*right.imaginary_;
	if(thisVolume > rightVolume)	return true;
	return false;
}

ostream& operator<<(ostream& cout, const ComplexNumber& complexNumber){
	return cout << complexNumber.real_ << " + " << complexNumber.imaginary_ << "i";
}

ComplexNumber operator+(int left, const ComplexNumber& right){
	return right + left;
}
ComplexNumber operator-(int left, const ComplexNumber& right){
	return ComplexNumber(left - right.real_, -right.imaginary_);
}