#pragma once

template <typename T>
class generator{
	T startingEntity_;
	T currentEntity_;
	T (*incrementer_)(T);
public:
	generator(T startingEntity, T (*incrementer)(T));

	T current() const;
	T next();
	bool operator<(T comparator) const;
	bool operator>(T comparator) const;

	void operator++();
	void reset();
	void reset(T startingEntity, T (*incrementer)(T));
};

template <typename T>
generator<T>::generator(T startingEntity, T (*incrementer)(T)): startingEntity_(startingEntity), currentEntity_(startingEntity), incrementer_(incrementer){}

template <typename T>
T generator<T>::current() const{
	return currentEntity_;
}

template <typename T>
T generator<T>::next(){
	++(*this);
	return currentEntity_;
}

template <typename T>
bool generator<T>::operator<(T comparator) const{
	return currentEntity_ < comparator;
}

template <typename T>
bool generator<T>::operator>(T comparator) const{
	return currentEntity_ > comparator;
}

template <typename T>
void generator<T>::operator++(){
	currentEntity_ = incrementer_(currentEntity_);
}

template <typename T>
void generator<T>::reset(){
	currentEntity_ = startingEntity_;
}

template <typename T>
void generator<T>::reset(T startingEntity, T (*incrementer)(T)){
	currentEntity_ = startingEntity_ = startingEntity;
	incrementer_ = incrementer;
}