#pragma once
#include <iostream>
#include <vector>

using std::ostream;
using std::cout;
using std::endl;
using std::string;
using std::vector;

class Stan{
	string stan_;
public:
	Stan(const char *stan);

	void operator()() const;
	const string& getString() const;
};

class MaszynaStanow{
	vector<Stan*> stany_;
	unsigned current_;
public:
	void start();
	void operator++();
	void operator--();
	void operator+=(Stan *stan);

	const Stan& operator()() const;

	~MaszynaStanow();

	friend ostream& operator<<(ostream& cout, const MaszynaStanow& m);
};