#include "MaszynaStanow.h"

Stan::Stan(const char *stan):stan_(stan){}

void Stan::operator()() const{
	cout << "Wykonanie w stanie: " << stan_ << endl;
}

const string& Stan::getString() const{
	return stan_;
}

void MaszynaStanow::start(){
	current_ = 0;
}

void MaszynaStanow::operator++(){
	if(current_ >= stany_.size() - 1)	return;
	current_++;
}

void MaszynaStanow::operator--(){
	if(current_) current_--;
}

void MaszynaStanow::operator+=(Stan *stan){
	stany_.push_back(stan);
}

const Stan& MaszynaStanow::operator()() const{
	return *stany_[current_];
}

MaszynaStanow::~MaszynaStanow(){
	for(int i=0; i<stany_.size(); i++){
		delete stany_[i];
	}
}

ostream& operator<<(ostream& cout, const MaszynaStanow& m){
	cout << "MaszynaStanow:" << endl;
	for(int i=0; i<m.stany_.size(); i++){
		cout << i << " " << m.stany_[i]->getString();
		if(i == m.current_)	cout << " <";
		cout << endl;
	}
	return cout;
}


