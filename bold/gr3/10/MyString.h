#pragma once

class MyString{
	char *string_;
	char delim_;
public:
	MyString();
	MyString(const char *string);

	operator char*() const;
	MyString operator() (int start, int end) const;

	void operator<< (MyString& rightString);
	void operator>> (MyString& rightString);
	void setDelim(char delim);

	~MyString();
};