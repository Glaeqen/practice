#include <cstring>

#include "MyString.h"

MyString::MyString():string_(0), delim_(' '){}

MyString::MyString(const char *string){
	string_ = new char[strlen(string)+1];
	strcpy(string_, string);
	delim_ = ' ';
}

MyString::operator char*() const{
	return string_;
}

MyString MyString::operator() (int start, int end) const{
	char *tempString = new char[end-start+1];
	for(int iterator = start; iterator < end-start; iterator++){
		tempString[iterator] = string_[iterator];
	}
	tempString[end-start] = '\0';

	MyString myString(tempString);
	delete[] tempString;

	return myString;
}

void MyString::operator<< (MyString& fromString){
	if(!fromString.string_)	return;
	char *firstEncounteredDelimPtr = index(fromString.string_, fromString.delim_);
	if(!firstEncounteredDelimPtr){
		firstEncounteredDelimPtr = fromString.string_ + strlen(fromString.string_);
	}
	
	int fromStringLength = strlen(fromString.string_) + 1;
	int newToStringLength = firstEncounteredDelimPtr - fromString.string_ + 1;
	int newFromStringLength = fromStringLength - newToStringLength;

	char *newToString;
	char *newFromString;

	newToStringLength ? newToString = new char[newToStringLength] : newToString = NULL;
	newFromStringLength ? newFromString = new char[newFromStringLength] : newFromString = NULL;

	char *fromStringIter = fromString.string_;
	char *newToStringIter = newToString;
	char *newFromStringIter = newFromString;
	while(*fromStringIter){
		if(fromStringIter < firstEncounteredDelimPtr){
			*newToStringIter++ = *fromStringIter;
		}
		if(fromStringIter > firstEncounteredDelimPtr){
			*newFromStringIter++ = *fromStringIter;

		}
		fromStringIter++;
	}

	if(newToStringLength) *newToStringIter = '\0';
	if(newFromStringLength) *newFromStringIter = '\0';

	delete[] string_;
	string_ = newToString;

	delete[] fromString.string_;
	fromString.string_ = newFromString;
}

void MyString::operator>> (MyString& toString){
	if(!string_)	return;
	char *lastEncounteredDelimPtr = rindex(string_, delim_);
	if(!lastEncounteredDelimPtr){
		lastEncounteredDelimPtr = string_ + strlen(string_);
	}

	int fromStringLength = strlen(string_) + 1;
	int newFromStringLength = lastEncounteredDelimPtr - string_ + 1;
	int newToStringLength = fromStringLength - newFromStringLength;

	char *newFromString;
	char *newToString;

	newFromStringLength ? newFromString = new char[newFromStringLength] : newFromString = NULL;
	newToStringLength ? newToString = new char[newToStringLength] : newToString = NULL;

	char *fromStringIter = string_;
	char *newFromStringIter = newFromString;
	char *newToStringIter = newToString;
	while(*fromStringIter){
		if(fromStringIter < lastEncounteredDelimPtr){
			*newFromStringIter++ = *fromStringIter;
		}
		if(fromStringIter > lastEncounteredDelimPtr){
			*newToStringIter++ = *fromStringIter;

		}
		fromStringIter++;
	}

	if(newFromStringLength) *newFromStringIter = '\0';
	if(newToStringLength) *newToStringIter = '\0';

	delete[] string_;
	string_ = newFromString;

	delete[] toString.string_;
	toString.string_ = newToString;
}
	
void MyString::setDelim(char delim){
	delim_ = delim;
}

MyString::~MyString(){
	delete[] string_;
}