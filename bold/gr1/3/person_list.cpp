#include <cstring>

#include "person.h"
#include "person_list.h"


void init_person_list(person_list *person_list){
	person_list->person = NULL;
	person_list->next = NULL;
}

void add_person(person_list *pList, char *buffer_name, char *buffer_family){
	int name_length = strlen(buffer_name)+1;
	int family_length = strlen(buffer_family)+1;

	if(pList->person == NULL){
		pList->person = new Person;
		pList->person->name = new char[name_length];
		pList->person->family = new char[family_length];
		strcpy(pList->person->name, buffer_name);
		strcpy(pList->person->family, buffer_family);
		return;
	}
	for(;pList->next!=NULL; pList = pList->next);
	
	pList->next = new person_list;
	pList->next->person = new Person;
	pList->next->person->name = new char[name_length];
	pList->next->person->family = new char[family_length];

	pList->next->next = NULL;

	strcpy(pList->next->person->name, buffer_name);
	strcpy(pList->next->person->family, buffer_family);
}

person_list_iter get_iterator(person_list *pList){
	person_list_iter iter;
	iter.name = pList->person->name;
	iter.family = pList->person->family;
	iter.currentNext = pList->next;
	return iter;
}

bool is_valid(person_list_iter pListIter){
	return pListIter.name != NULL ? true : false;
}

void move_to_next(person_list_iter *pListIter){
	if(pListIter->currentNext != NULL){
		pListIter->name = pListIter->currentNext->person->name;
		pListIter->family = pListIter->currentNext->person->family;
		pListIter->currentNext = pListIter->currentNext->next;
	}
	else{
		pListIter->name = NULL;
		pListIter->family = NULL;
	}
}

void cleanNode(person_list *list_node){
	delete[] list_node->person->name;
	delete[] list_node->person->family;
	delete list_node->person;
}

void clean(person_list *pList){
	person_list *tempPList;

	cleanNode(pList);
	pList=pList->next;

	while(pList){
		tempPList=pList;
		pList=pList->next;
		
		cleanNode(tempPList);
		delete tempPList;
	}
}
