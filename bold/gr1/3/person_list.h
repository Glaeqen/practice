#pragma once

#include "person.h"

typedef struct person_list person_list;
typedef struct person_list_iter person_list_iter;

struct person_list{
	Person *person;
	person_list *next;
};

struct person_list_iter{
	char *name;
	char *family;
	person_list *currentNext;
};

void init_person_list(person_list *person_list);
void add_person(person_list *person_list, char *buffer_name, char *buffer_family);

person_list_iter get_iterator(person_list *person_list);
bool is_valid(person_list_iter person_list_iter);
void move_to_next(person_list_iter *person_list_iter);

void clean(person_list *person_list);
