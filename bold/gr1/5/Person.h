#pragma once

#include <cstring>

class Person{
private:
	char* name_;
	char* family_;

public:
	Person(char* name, char* family){
		int nameLength = strlen(name) + 1;
		int familyLength = strlen(family) + 1;

		name_ = new char[nameLength];
		family_ = new char[familyLength];

		strcpy(name_, name), strcpy(family_, family);
	}

	char* name()const{
		return name_;
	}

	char* family()const{
		return family_;
	}

	~Person(){
		delete[] name_;
		delete[] family_;
	}
};