#pragma once

class Person;

class PersonList{
public:
	class Iterator{
	private:
		Person* person_;
		PersonList* next_;
		bool isValid_;

	public:
		Iterator(Person* person, PersonList* ptrToNextIter);

		const Person& obj();
		void moveToNext();
		bool isValid();
	};

private:
	Person* person_;
	PersonList* next_;

public:
	PersonList();

	void addPerson(char* name, char* family);
	PersonList::Iterator getIterator() const;
	void copyPerson(char* name, char* family);

	~PersonList();
};
