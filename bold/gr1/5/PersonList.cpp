#include <cstring>

#include "Person.h"
#include "PersonList.h"

PersonList::Iterator::Iterator(Person* person, PersonList* ptrToNextIter){
	person_ = person;
	next_ = ptrToNextIter;
	isValid_ = true;
}

const Person& PersonList::Iterator::obj(){
	return *person_;
}

void PersonList::Iterator::moveToNext(){
	next_ = next_->next_;
	if(!next_){
		isValid_ = false;
		return;
	}
	person_ = next_->person_;
}

bool PersonList::Iterator::isValid(){
	return isValid_;
}



PersonList::PersonList(){
	person_ = NULL;
	next_ = NULL;
}

void PersonList::addPerson(char* name, char* family){
	if(!person_ && !next_){
		person_ = new Person(name, family);
		return;
	}

	PersonList *tempPtr = this;
	for(; tempPtr->next_ != NULL; tempPtr = tempPtr->next_);
	tempPtr->next_ = new PersonList;
	tempPtr->next_->person_ = new Person(name, family);
	tempPtr->next_->next_ = NULL;
}

PersonList::Iterator PersonList::getIterator() const{
	Iterator iter(person_, next_);
	return iter;
}

PersonList::~PersonList(){
	delete person_;
	delete next_;
}
