#include <string.h>
#include "strop.h"

void chunk(const char *string, delimited *d, const char delimiter){
	int stringLength = strlen(string);
	strcpy(d->string, string);
	prepareArray(d->delimiterPositions);
	findDelimitersPos(string, d, delimiter, stringLength);
}

void prepareArray(int *array){
	for(int i=0; i<256; i++){
		array[i] = 0;
	}
}

void findDelimitersPos(const char *string, delimited *d, const char delimiter, int stringLength){
	int delimiterCounter = 1;
	for(int i=0; i<stringLength; i++){
		if(string[i]==delimiter){
			d->delimiterPositions[delimiterCounter] = i;
			delimiterCounter++;
		}
	}

	//Prepare first partitioning
	d->delimiterPositions[delimiterCounter] = stringLength;
	d->startingPos = 0;
	d->endingPos = 1;
}

void part(delimited *d, char *buffer){
	clearBuffer(buffer);
	cpyChunkToBuffer(d, buffer);
	prepareNextPartition(d);
}

void clearBuffer(char *buffer){
	for(int i=0; i<256; i++){
		buffer[i] = 0;
	}
}

void cpyChunkToBuffer(delimited *d, char *buffer){
	int start = d->delimiterPositions[d->startingPos];
	int end = d->delimiterPositions[d->endingPos];
	for(int i = start, bufferCounter = 0; i<end; i++, bufferCounter++){
		buffer[bufferCounter] = d->string[i];
	}
}

void prepareNextPartition(delimited *d){
	d->startingPos++;
	d->endingPos++;
	d->delimiterPositions[d->startingPos]++;
}


bool next(delimited *d){
	if(d->delimiterPositions[d->endingPos])	return true;
	return false;
}
