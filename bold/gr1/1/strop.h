#ifndef _STROP_
#define _STROP_

typedef struct tagDelimited{
	char string[256];
	int delimiterPositions[256];
	int startingPos;
	int endingPos;
}delimited;

void chunk(const char *string, delimited *d, const char delimiter);
void prepareArray(int *array);
void findDelimitersPos(const char *string, delimited *d, const char delimiter, int stringLength);

void part(delimited *d, char *buffer);
void clearBuffer(char *buffer);
void cpyChunkToBuffer(delimited *d, char *buffer);
void prepareNextPartition(delimited *d);

bool next(delimited *d);

#endif	