#ifndef _EXT_
#define _EXT_

typedef struct Ext{
	int *array;
	int currentSize;
	int (*capacityPolicy)(int);
	int currentCapacity;
}Ext;

void init(Ext *vector);
void init_with_size_and_policy(Ext *vector, int initCapacity, int (*policy)(int));

void insert(Ext *vector, int value);
int size(Ext *vector);
int capacity(Ext *vector);
int at(Ext *vector, int position);

Ext *clone(Ext *vector);

void for_each_element(Ext vector, void (*func)(int *));
void for_each_element(Ext *vector, void (*func)(int *));

void print(int *element);
void zero(int *element);

void clear(Ext *vector);
void clear(Ext vector);

int defaultPolicy(int sz);

#endif