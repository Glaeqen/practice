#include <stdlib.h>
#include <stdio.h>

#include "ext.h"

#define DEFAULT_SIZE 5	// Wiem, że define jest globalne, ale przekazywalem
						// jako argument do funkcji w ramach estetyki kodu.

void init(Ext *vector){
	init_with_size_and_policy(vector, DEFAULT_SIZE, defaultPolicy);
}

void init_with_size_and_policy(Ext *vector, int initCapacity, int (*policy)(int)){
	vector->array = (int *)malloc(initCapacity*sizeof(vector->array[0]));
	vector->currentSize = 0;
	vector->capacityPolicy = policy; 
	vector->currentCapacity = initCapacity;
}

void insert(Ext *vector, int value){
	if(vector->currentSize >= vector->currentCapacity){
		vector->currentCapacity = vector->capacityPolicy(vector->currentCapacity);
		vector->array = (int *)realloc(vector->array, sizeof(vector->array[0])*vector->currentCapacity);
	}
	vector->array[vector->currentSize] = value;
	vector->currentSize++;
}

int size(Ext *vector){
	return vector->currentSize;
}

int capacity(Ext *vector){
	return vector->currentCapacity;
}

int at(Ext *vector, int position){
	if(position>=vector->currentSize){
		puts("\'at\' method asks for values out of array.");
		return -1;
	}
	return vector->array[position];
}

Ext *clone(Ext *vector){
	//Ext *newVector = (Ext *)malloc(sizeof(*newVector)); << Nie zadziała bo Bołd użył delete'a w main'ie
	Ext *newVector = new Ext;
	newVector->currentSize = vector->currentSize;
	newVector->capacityPolicy = vector->capacityPolicy;
	newVector->currentCapacity = vector->currentCapacity;
	newVector->array = (int *)malloc(newVector->currentCapacity*sizeof(newVector->array[0]));
	for(int i=0; i<newVector->currentSize; i++){
		newVector->array[i] = vector->array[i];
	}
	return newVector;
}

void for_each_element(Ext vector, void (*func)(int *)){
	for(int i=0; i<vector.currentSize; i++){
		func(vector.array+i);
	}
}

void for_each_element(Ext *vector, void (*func)(int *)){
	for(int i=0; i<vector->currentSize; i++){
		func(vector->array+i);
	}
}

void print(int *element){
	printf("%d ", *element);
}

void zero(int *element){
	*element = 0;
}

void clear(Ext *vector){
	free(vector->array);
}

void clear(Ext vector){
	free(vector.array);
}


int defaultPolicy(int input){
	return DEFAULT_SIZE+input;
}