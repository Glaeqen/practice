#pragma once

struct Byte{
	bool a:1;
	bool b:1;
	bool c:1;
	bool d:1;
	bool e:1;
	bool f:1;
	bool g:1;
	bool h:1;
};

struct BitArray{
	Byte *array;
	unsigned int rows;
	unsigned int columns;
	unsigned int amountOfInvalidBitsAtTheEnd;
	unsigned int realVolume;
};

void fillZeroArray(BitArray *bitArray);

unsigned int getBitInByte(const Byte byte, unsigned int position);
void setBitInByte(Byte &byte, unsigned int position, bool bitValue);
void negateBitInByte(Byte &byte, unsigned int position);

void init(BitArray *bitArray, unsigned int rows, unsigned int columns);
void set(BitArray *bitArray, int row, int column, bool bitValue);

void print(const BitArray *bitArray, const char *name);
void clear(BitArray *bitArray);

unsigned int rows(const BitArray *bitArray);
unsigned int cols(const BitArray *bitArray);

BitArray *negate(BitArray *bitArray);
void xor_arrays(BitArray *bitArrayOutput, BitArray const *bitArray1, BitArray const *bitArray2);
