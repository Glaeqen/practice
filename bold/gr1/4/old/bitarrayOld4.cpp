#include <cmath>
#include "bitarray.h"


void init(BitArray *bitArray, unsigned int rows, unsigned int columns){
	int arraySize = (int)ceil(rows*columns/8.0);

	bitArray->realVolume = arraySize;
	bitArray->rows = rows;
	bitArray->columns = columns;
	bitArray->array = new unsigned char[arraySize];
}

void set(BitArray *bitArray, int row, int column, bool bitValue){
	int positionInByteFromBit = (int)((cols(bitArray)*row+column)/8.0);
	unsigned char& byteToWorkOn = bitArray->array[positionInByteFromBit];

	if(bitValue) byteToWorkOn = byteToWorkOn | (unsigned char)pow(2, 7-column);
	
	else byteToWorkOn = byteToWorkOn & (unsigned char)(255- pow(2, 7-column));
}

void strtacBin(char *output, char *input){
	for(int i=0; i<8; i++){
		output[i] = input[7-i];
	}
}

void decToReversedBinInChar(unsigned char inputChar, char *buffer, int bufferIndex){
	bufferIndex++;
	if (inputChar / 2 != 0) {
        decToReversedBinInChar(inputChar / 2, buffer, bufferIndex);
    }

    buffer[bufferIndex] = inputChar%2+48;
}

void decToBinInChar(char *output, unsigned char inputChar){
	char tempString[9] = {0};
	decToReversedBinInChar(inputChar, tempString, -1);
	strtacBin(output, tempString);
}

void print(const BitArray *bitArray, const char *name){
	int tempArrayIndex = 0;
	int tempArraySize = ceil(rows(bitArray)*cols(bitArray)*1.0);
	char *tempArray = new char[tempArraySize];

	for(int i=0; i<tempArraySize; i++){
		tempArray[i] = 0;
	}

	char tempString[9] = {0};
	for(unsigned int i=0; i<bitArray->realVolume; i++){
		decToBinInChar(tempString, bitArray->array[i]);
		for(int j=0; j<8; j++, tempArrayIndex+=8){
			tempArray[tempArrayIndex] = tempString[j];
		}
	}
	puts(name), putchar('\n');
	for(int i=0; i<tempArraySize; i++){
		putchar(tempArray[i]);
		if(!i%cols(bitArray)) putchar('\n');
	}
}

void clear(BitArray *bitArray){
	if(!bitArray->array)	return;
	delete[] bitArray->array;
	bitArray->array = NULL;
}

unsigned int rows(const BitArray *bitArray){
	return bitArray->rows;
}

unsigned int cols(const BitArray *bitArray){
	return bitArray->columns;
}

BitArray *negate(BitArray *bitArray){
	for(unsigned int i=0; i<bitArray->realVolume; i++){
		bitArray->array[i] = ~(bitArray->array[i]);
	}

	return bitArray;
}

void xor_arrays(BitArray *bitArrayOutput, BitArray const *bitArray1, BitArray const *bitArray2){
	for(unsigned int i=0; i<bitArray1->realVolume; i++){
		bitArrayOutput->array[i] = bitArray1->array[i] ^ bitArray2->array[i];
	}
}