#include <iostream>
#include <cstdio>
#include <cmath>

#include "bitarray.h"

void fillZeroArray(BitArray *bitArray){
	for(unsigned int i=0; i<bitArray->realVolume; i++){
		for(unsigned int j=0; j<8; j++){
			setBitInByte(bitArray->array[i], j, false);
		}
	}
}

unsigned int getBitInByte(const Byte byte, unsigned int position){
	switch(position){
		case 0:
			return byte.a;
		case 1:
			return byte.b;
		case 2:
			return byte.c;
		case 3:
			return byte.d;
		case 4:
			return byte.e;
		case 5:
			return byte.f;
		case 6:
			return byte.g;
		case 7:
			return byte.h;
		default:
			puts("Postion > 7, getBitInByte returning..");
			return 0;
	}
}

void setBitInByte(Byte &byte, unsigned int position, bool bitValue){
	switch(position){
		case 0:
			byte.a = bitValue;
			break;
		case 1:
			byte.b = bitValue;
			break;
		case 2:
			byte.c = bitValue;
			break;
		case 3:
			byte.d = bitValue;
			break;
		case 4:
			byte.e = bitValue;
			break;
		case 5:
			byte.f = bitValue;
			break;
		case 6:
			byte.g = bitValue;
			break;
		case 7:
			byte.h = bitValue;
			break;
		default:
			puts("Postion > 7, setBitInByte returning..");
			return;
	}
}

void negateBitInByte(Byte &byte, unsigned int position){
	if(getBitInByte(byte, position))	setBitInByte(byte, position, false);
	else	setBitInByte(byte, position, true);

}

void init(BitArray *bitArray, unsigned int rows, unsigned int columns){
	unsigned int realVolume = ceil(rows*columns/8.0);

	bitArray->rows = rows;
	bitArray->columns = columns;
	bitArray->realVolume = realVolume;
	bitArray->amountOfInvalidBitsAtTheEnd = realVolume*8 - rows*columns;

	bitArray->array = new Byte[realVolume];
	fillZeroArray(bitArray);
}

void set(BitArray *bitArray, int row, int column, bool bitValue){
	int positionInBits = row*cols(bitArray)+column;
	int positionInArray = (int)(positionInBits/8.0);

	Byte &byteToChange = bitArray->array[positionInArray];

	setBitInByte(byteToChange, positionInBits%8, bitValue);
}

void print(const BitArray *bitArray, const char *name){
	unsigned int columns = cols(bitArray);
	unsigned int realVolume = bitArray->realVolume;
	unsigned int invalidBits = bitArray->amountOfInvalidBitsAtTheEnd;

	std::cout << name << std::endl;
	for(unsigned int byte=0; byte<realVolume; byte++){
		for(unsigned int bitPos=0; bitPos<8; bitPos++){
			if(realVolume*8 - invalidBits == byte*8+bitPos)	break;

			std::cout << getBitInByte(bitArray->array[byte], bitPos);

			if(!((byte*8+bitPos+1)%(columns))) std::cout << std::endl;
		}
	}
}

void clear(BitArray *bitArray){
	if(bitArray->array == NULL)	return;
	delete[] bitArray->array;
	bitArray->array = NULL;
}

unsigned int rows(const BitArray *bitArray){
	return bitArray->rows;
}

unsigned int cols(const BitArray *bitArray){
	return bitArray->columns;
}

BitArray *negate(BitArray *bitArray){
	for(unsigned int byte=0; byte<bitArray->realVolume; byte++){
		for(unsigned int bitPos=0; bitPos<8; bitPos++){
			negateBitInByte(bitArray->array[byte], bitPos);
		}
	}
	return bitArray;
}

void xor_arrays(BitArray *bitArrayOutput, BitArray const *bitArray1, BitArray const *bitArray2){
	unsigned int bitInArray1;
	unsigned int bitInArray2;
	for(unsigned int byte=0; byte<bitArray1->realVolume; byte++){
		for(unsigned int bitPos=0; bitPos<8; bitPos++){
			bitInArray1 = getBitInByte(bitArray1->array[byte], bitPos);
			bitInArray2 = getBitInByte(bitArray2->array[byte], bitPos);
			setBitInByte(bitArrayOutput->array[byte], bitPos, bitInArray1 ^ bitInArray2);
		}
	}
}
