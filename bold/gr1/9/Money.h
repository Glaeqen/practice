#pragma once
#include <string>

enum Operation{
	_10 = 10,
	_100 = 100,
	_1000 = 1000
};

class Money;

struct Currency{
	static const std::string USD;
	static const std::string PLN;

	static std::string type(const Money& money);
};

class Money{
	int money;
	std::string currency;
	Money(int money, std::string currency);
public:
	int operator+=(Operation operation) const;
	int operator-=(Operation operation) const;

	int getMoney() const;
	std::string getCurrency() const;

	friend Money operator*(int a, const std::string& b);
};

Money operator*(int a, const std::string& b);
