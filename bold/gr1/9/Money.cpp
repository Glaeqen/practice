#include <cmath>

#include "Money.h"

const std::string Currency::USD("USD");
const std::string Currency::PLN("OKB");

std::string Currency::type(const Money& money){
	return money.getCurrency();
}

Money::Money(int money, std::string currency):money(money), currency(currency){}


int Money::operator+=(Operation operation) const{
	return (money/operation+1)*operation;
}

int Money::operator-=(Operation operation) const{
	return (money/operation)*operation;
}

int Money::getMoney() const{
	return money;
}

std::string Money::getCurrency() const{
	return currency;
}

Money operator*(int a, const std::string& b){
	return Money(a, b);
}
