#include <string>

#include "LineFit.h"

FitResult LineFit::result() const{
	std::string expression = " y = a * x + b";
	unsigned nParams = 2;

	std::string *parameterName = new std::string[2];
	parameterName[0] = "a";
	parameterName[1] = "b";

	float *parameterValue = new float[2];
	parameterValue[0] = (sumXY - (sumX*sumY)/pointsAmount)/(sumXX - (sumX*sumX)/pointsAmount);
	parameterValue[1] = (sumY - parameterValue[0]*sumX)/pointsAmount;

	return FitResult(expression, nParams, parameterName, parameterValue);
}