#pragma once
#include "Fit.h"

class SlopeFit : public Fit{
public:
	FitResult result() const;
};