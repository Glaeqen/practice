#pragma once
#include "Fit.h"

class LineFit : public Fit{
public:
	FitResult result() const;
};