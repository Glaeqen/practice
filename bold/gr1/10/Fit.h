#pragma once

class FitResult{
	std::string expression_;
	unsigned nParams_;
	std::string *parameterName_;
	float *parameterValue_;
public:
	FitResult(const std::string& expression, unsigned nParams, std::string *parameterName, float *parameterValue);

	const std::string& expression() const;
	unsigned nParams() const;
	const std::string& parameterName(unsigned i) const;
	const float& parameterValue(unsigned i) const;

	~FitResult();
};

class Fit{
protected:
	float sumX;
	float sumY;
	float sumXY;
	float sumXX;
	unsigned pointsAmount;

	Fit();
public:
	void appendPoint(float x, float y);
	virtual FitResult result() const = 0;
};