#include <string>

#include "Fit.h"

// class FitResult

FitResult::FitResult(const std::string& expression, unsigned nParams, std::string *parameterName, float *parameterValue):
expression_(expression), nParams_(nParams), parameterName_(parameterName), parameterValue_(parameterValue){}

const std::string& FitResult::expression() const{
	return expression_;
}

unsigned FitResult::nParams() const{
	return nParams_;
}

const std::string& FitResult::parameterName(unsigned i) const{
	return parameterName_[i];
}

const float& FitResult::parameterValue(unsigned i) const{
	return parameterValue_[i];
}

FitResult::~FitResult(){
	delete[] parameterName_;
	delete[] parameterValue_; 
}

// class Fit

Fit::Fit(){
	sumX = sumY = sumXY = sumXX = 0.0;
	pointsAmount = 0;
}

void Fit::appendPoint(float x, float y){
	sumX += x;
	sumY += y;
	sumXY += x*y;
	sumXX += x*x;
	pointsAmount++;
}