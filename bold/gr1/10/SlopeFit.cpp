#include <string>

#include "SlopeFit.h"

FitResult SlopeFit::result() const{
	std::string expression = " y = a * x";
	unsigned nParams = 1;

	std::string *parameterName = new std::string[1];
	parameterName[0] = "a";

	float *parameterValue = new float[1];
	parameterValue[0] = sumXY / sumXX;

	return FitResult(expression, nParams, parameterName, parameterValue);
}