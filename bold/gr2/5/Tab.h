#pragma once

#include "TabRange.h"

class Tab{
private:
	double** _array;
	int _rows;
	int _columns;

	Tab();
	void init(int rows, int columns, double initValue);
public:
	Tab(int rows, int columns, double initValue);
	Tab(const Tab& tab, const TabRange& tabRange);

	void diag(double value);
	Tab& set(int row, int column, double value);
	void setPart(const TabRange& tabRange, int value);

	int getRows() const{
		return _rows;
	}
	int getColumns() const{
		return _columns;
	}
	double at(int row, int column) const{
		return _array[row][column];
	}
	void print() const;

	~Tab();
};

