/* 
  Celem zadania jest napisanie klasy reprezenujaca tablice dwuwymiarowa.
  Wazne: stworzenie tablicy nie moze skladac sie z pojedynczej instrukcji, 
  to ma byc tablica tablic.
  
  Klassa Tab powinna byc wyposazona w  nietrywialne konstruktory i metody:
  diag (do ustawienia wartosci wszystkich el. diagonalnych) ustawienia
  elementow w kolumnie i wierszu a takze do wypisania tablicy.  
  Uwaga: przy implementacji powyzszych prosze sie zastanowic, ktora z funkcji
  mozna uzyc do implementacji innych.

   
  Z uzyciem pomocniczej klasy TabRange 
  (zespol 4 indeksow wiersz, kolumna, wiersz, kolumna)
  powinnismy moc ustawiac wartosci w grupach elementow.  

  Takze z uzyciem struktury TabRange powinnismy moc stworzyc nowa tablice z
  czesci elementow innej tablicy jak pokazuje przyklad.
  
  Uwaga: nalezy zadbac o poprawnosc const nawet tam gdzie nie wymaga tego przyklad. 
  Uwaga: to jest obiektowy wariant poprzedniego zadania, prosze zwrocic jednak uwage na 
     to ze TabRange inaczej indeksuje elementy (cf. kreacja ostatniej tablicy).
  Program nalezy skompilowac do pliku wkonywalnego o nazwie tabtest z flagami -g -Wall
*/
#include <iostream>
using namespace std;

#include "Tab.h"
#include "Tab.h"

int main() {
  Tab t(7,7, 0.1); // tablica 7 x 7 zainicjalizowana wartosciam 0.1

  t.diag(3.2);
  t.set( 6, 6, 7.5).set(6,5, 8.6f);
  
  t.setPart(TabRange(1,1, 3,3), 999);
  t.set( 6, 4, t.at(1,1));
  t.print();
  
  cout << "jakas inna tablica (niesymetryczna)" << endl;
  Tab t2(3, 7, 2.);
  t2.set( 2, 2, 4.);
  t2.print();

  cout << "wycinamy srodek (asymetrycznie) jako nowa tablice" << endl;
  const TabRange center(0,2, 6,4);  
  Tab c(t, center);
  const Tab& d = c;
  d.print();
}
/* Wynik
| 3.2 0.1 0.1 0.1 0.1 0.1 0.1 |
| 0.1 999 999 0.1 0.1 0.1 0.1 |
| 0.1 999 999 0.1 0.1 0.1 0.1 |
| 0.1 0.1 0.1 3.2 0.1 0.1 0.1 |
| 0.1 0.1 0.1 0.1 3.2 0.1 0.1 |
| 0.1 0.1 0.1 0.1 0.1 3.2 0.1 |
| 0.1 0.1 0.1 0.1 999 8.6 7.5 |

jakas inna tablica (niesymetryczna)
| 2 2 2 2 2 2 2 |
| 2 2 2 2 2 2 2 |
| 2 2 4 2 2 2 2 |

wycinamy srodek (asymetrycznie) jako nowa tablice
| 0.1 0.1 |
| 999 0.1 |
| 999 0.1 |
| 0.1 3.2 |
| 0.1 0.1 |
| 0.1 0.1 |


 */
