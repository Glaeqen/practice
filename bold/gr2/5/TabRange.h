#pragma once

class Point{
private:
	int _x;
	int _y;
public:
	void setX(int x){
		_x = x;
	}
	void setY(int y){
		_y = y;
	}

	int getX() const{
		return _x;
	}
	int getY() const{
		return _y;
	}
};

class TabRange{
private:
	Point _begin;
	Point _end;
	TabRange();
public:
	TabRange(int beginX, int beginY, int endX, int endY);
	const Point& getBegin() const{
		return _begin;
	}
	const Point& getEnd() const{
		return _end;
	}
};