#include <iostream>

#include "Tab.h"

Tab::Tab(int rows, int columns, double initValue){
	init(rows, columns, initValue);
}

Tab::Tab(const Tab& tab, const TabRange& tabRange){
	int newRows = tabRange.getEnd().getX() - tabRange.getBegin().getX();
	int newColumns = tabRange.getEnd().getY() - tabRange.getBegin().getY();
	init(newRows, newColumns, 0.0);

	for(int i=0; i<_rows; i++){
		for(int j=0; j<_columns; j++){
			_array[i][j] = tab.at(tabRange.getBegin().getX()+i, tabRange.getBegin().getY()+j);
		}
	}
}

void Tab::init(int rows, int columns, double initValue){
	_array = new double*[rows];
	for(int i=0; i<rows; i++){
		_array[i] = new double[columns];
		for(int j=0 ; j<columns; j++){
			_array[i][j] = initValue;
		}
	}
	_rows = rows;
	_columns = columns;
}

void Tab::diag(double value){
	if(_rows!=_columns){
		std::cout << "diag(): Rows =! columns. Returning.";
		std::cout << std::endl;
	}
	for(int i=0; i<_rows; i++){
		_array[i][i] = value;
	}
}

Tab& Tab::set(int row, int column, double value){
	_array[row][column] = value;
	return *this;
}

void Tab::setPart(const TabRange& tabRange, int value){
	Point upperLeftCorner = tabRange.getBegin();
	Point lowerRightCorner = tabRange.getEnd();
	for(int i=upperLeftCorner.getX(); i<lowerRightCorner.getX(); i++){
		for(int j=upperLeftCorner.getY(); j<lowerRightCorner.getY(); j++){
			_array[i][j] = value*1.0;
		}
	}
}

void Tab::print() const{
	using std::cout;
	using std::endl;
	for(int i=0; i<_rows; i++){
		cout << "| ";
		for(int j=0; j<_columns; j++){
			cout << _array[i][j] << " ";
		}
		
		cout << "|" << endl;
	}
}

Tab::~Tab(){
	for(int i=0; i<_rows; i++){
		delete[] _array[i];
	}
	delete[] _array;
}