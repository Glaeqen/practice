#include "value.h"

int value::$ = 0;

void value::integer::operator-(int value){
	this->value_ -= value;
	value::$ = this->value_;
}

void value::integer::operator*(int value){
	this->value_ *= value;
	value::$ = this->value_;
}

void value::integer::operator*(const integer& integer){
	this->value_ *= integer.value_;
	value::$ = this->value_;
}

void value::integer::operator/(int value){
	this->value_ /= value;
	value::$ = this->value_;
}

ostream& value::operator<<(ostream& cout, const integer& integer){
	return cout << integer.value_;
}