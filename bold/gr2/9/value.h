#pragma once

#include <iostream>

using std::ostream;
using std::cout;
using std::endl;

namespace value{
	extern int $;

	class integer{
		friend ostream& operator<<(ostream& cout, const integer& integer);
		
		int value_;
	public:
		integer(int value):value_(value){}

		int get() const{ return value_; }
		void operator-(int value);
		void operator*(int value);
		void operator*(const integer& integer);
		void operator/(int value);
	};

	ostream& operator<<(ostream& cout, const integer& integer);
}

