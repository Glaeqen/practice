// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze napisac szablony obliczajace najwiekszy wspolny dzielnik
// dwoch liczb naturalnych. Obliczanie NWD, ma odbywać się na etapie
// kompilaji. Wskazniki na obiekty przechowujace NWD powinno dac sie
// przechowywac w jednej tablicy. (prosze zwrocic uwage na gwiazdke w
// linii 42: NWDTAB_T* tablica [rozmiar];

// Prosze zadbac o to, aby zwolnic pamiec po obiektach NWD.

// Pliku lab10.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie lab10. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_10

#include <iostream>
#include "lab10.h"


int main() {
  using namespace std;

  const int rozmiar = 3;
  NWDTAB_T* tablica [rozmiar];
  tablica[0] = new NWD<197, 37>;
  tablica[1] = new NWD<216, 128>;
  tablica[2] = new NWD<56, 28>;

  cout<<"Wypisywanie"<<endl;
  for (int i = 0; i < rozmiar; ++i)
    tablica[i]->Wypisz();

  cout<<"Koniec Wypisywania"<<endl;
}
/*
Wypisywanie
NWD 197, 37 wynosi 1
NWD 216, 128 wynosi 8
NWD 56, 28 wynosi 28
Koniec Wypisywania
*/
