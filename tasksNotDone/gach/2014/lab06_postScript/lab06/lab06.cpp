// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze zaimplementowac klase Canvas oraz klasy odpowiadajace trojkatowi, kwadratowi i prostokatowi.

// Klasa Canvas reprezentuje przestrzen na ktorej mozna rysowac. Gdy
// zostanie wywolana funkcja Draw zostanie narysowane obramowanie tej
// przesrzeni oraz zostana narysowane wszystkie elementy, ktore
// zostaly dodane do tej klasy. Prosze przygotowac klase tak, aby
// mozna bylo na niej rowniez inne obiekty, nie tylko trojkat,
// prostokat i kwadrat.

// Klasy przedstawiajace figury powinny rysowac wypelnione figury.

// Program powinien wypisywac na standardowe wyjscie kod PostScript
// odpowiadajacy zaprojektowanemu rysunkowi. Standardowe wyjscie mozna
// przekierowac do np. Rysunek.ps, a nastpnie obejzec rysunek przy
// pomocy programu graficznego (np. evince)


// ===== Wprowadzenie do jezyka PostScript =====

// Kazda nowa figura powinna rozpoczynac sie poleceniem:
// newpath

// Nastepnie nalezy umiescic kursor w miejscu z ktorego chce sie
// zaczac rysowac przy pomocy instrukcji
// x y moveto
// gdzie x i y to wspolrzedne punktu

// Aby przesunac kursor i narysowac linie laczaca stara pozycje
// kursora z nowa nalezy wykonac polecenie
// x_new y_new lineto
// gdzie x_new i y_new to wspolrzedne nowego polozenia kursora

// Aby narysowac kontury utworzone w powyzszy sposob nalezy wykonac
// polecenie
// stroke

// Aby wypelnic wnetrze utworzonej w powyzszy sposob figury nalezy
// wykonac polecenie
// fill

// ======================================================================

// Wyjscie jakie powinien generowac program jest podane na koncu tego
// pliku oraz w pliku Rysunek.ps, ktory mozna obejzec przy pomocy
// programu do wyswietlania grafiki.


// Pliku lab06.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab06. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_06


#include"lab06.h"
#include"iostream"

int main ()
{
  Canvas main (400, 300);	// construct canvas 400 px wide and 300 px high
  main.SetPos (20, 20);		// set position of lower left corner to (20, 20)

  // prepare and draw objects on left canvas
  Canvas left (180, 280);
  left.SetPos (30, 30);
  main.Add (left);		// draw left on the main canvas

  Rectangle leftRec (70, 100);	// construct rectangle
  leftRec.SetPos (120, 200);	// set position of lower left corner in absolute coordinates to (120, 200)
  left.Add (leftRec);
  
  Triangle leftTri (80);	// construct triangle base equal to 80 and height equal to 80
  leftTri.SetPos (45, 40);
  left.Add (leftTri);

  // prepare and draw objects on right canvas
  const int rightOffsetHor (220), rightOffsetVer(30);
  Canvas right (180, 280);
  right.SetPos (rightOffsetHor, rightOffsetVer);
  main.Add (right);		// draw right on the main canvas

  Square rightSq (90);	// construct square with side lenght equal 90
  rightSq.SetPos (rightOffsetHor + 25, rightOffsetVer + 10);	// set position of lower left corner in absolute coordinates
  right.Add (rightSq);						// draw square on the right canvas
  
  Triangle rightTri (100);
  rightTri.SetPos (rightOffsetHor + 20, rightOffsetVer + 90);
  right.Add (rightTri);

  Rectangle rightRec (10, 50);
  rightRec.SetPos (rightOffsetHor + 90, rightOffsetVer + 130);
  right.Add (rightRec);

  
  Rectangle notDrawn (100, 100); // this recntagle is not drawn, because it is not added to any canvas
  notDrawn.SetPos (0, 0);

  
  main.Draw();
  std::cout<<"showpage\n";

  Rectangle notDrawn2 (100, 100); // this recntagle is not drawn, because it is added to the canvas after it was drawn
  notDrawn2.SetPos (150, 150);
  right.Add(notDrawn2);
  
  return 0;
}
/*
newpath
20 20 moveto
420 20 lineto
420 320 lineto
20 320 lineto
20 20 lineto
stroke
newpath
30 30 moveto
210 30 lineto
210 310 lineto
30 310 lineto
30 30 lineto
stroke
newpath
120 200 moveto
190 200 lineto
190 300 lineto
120 300 lineto
120 200 lineto
fill
newpath
45 40 moveto
125 40 lineto
85 120 lineto
45 40 lineto
fill
newpath
220 30 moveto
400 30 lineto
400 310 lineto
220 310 lineto
220 30 lineto
stroke
newpath
245 40 moveto
335 40 lineto
335 130 lineto
245 130 lineto
245 40 lineto
fill
newpath
240 120 moveto
340 120 lineto
290 220 lineto
240 120 lineto
fill
newpath
310 160 moveto
320 160 lineto
320 210 lineto
310 210 lineto
310 160 lineto
fill
showpage
*/
