Prosze napisac program, ktory:

1.  wczyta dane z pliku tekstowego Dane.csv. Pierwszy
    wiersz zawiera informcje o liczbie kolumn w pliku. Kolumny
    oddzielone sa przecinkami. W drugim wierszu znajduje sie nazwa
    wielkosci oraz jej jednostka w nawiasie kwadratowym. Pozostale
    wiersze zawieraja wartosci. Ilosc wierszy powinna zostac obliczona
    przez program. Program powinien obslugiwac pliki o dowolnej
    liczbie wierszy.

2.  obliczy wartosc srednia w kazdej z kolumn

3.  obliczy niepewnosc standardowa wartosci sredniej dla kazdej z
    kolumn wedlug wzoru:
       u(x) = pierwiastek( suma( (x - x_sr)^2 ) / (n*(n-1)) )

4.  dla kazdej z kolumn wypisze na ekran oznaczenie wielkosci, jej
    wartosc srednia i jednostke (w dokladnie takiej
    kolejnosci). Nastepnie wypisze jej niepewnosc w takim samym
    formacie. Wyniki powinny byc odpowiednio zaokraglone
    tzn. niepewnosc bedzie podana do dwoch miejsc znaczacych, a wynik
    z taka sama dokladnoscia jak niepewnosc np.
    v = 230 m/s, u(v) = 120, 
    s = 121.2 m, u(s) = 1.1 m, 
    t = 0.0485 s, u(t) = 0.0012 s

5.  program powinien nazywac sie Lab03

Funkcje zamieniajace ciagi znakow na liczbe, to atoi, atof, dostpen w
bibliotece standardowej (cstdlib)
    
Przypominam, ze sa to zajecia z programowania obiektowego, wiec
program powinien byc zaprojektowany i napisany obiektowo.
