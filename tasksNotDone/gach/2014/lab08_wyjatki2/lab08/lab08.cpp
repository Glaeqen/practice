// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Proszę napisać klase Object, ktora przechowuje swoja
// nazwe. Konstruktor i destruktor tej klasy wypisuja komunikat jaki
// obiekt jest tworzony lub niszczony.

// Proszę także dopisać klasę, która będzie tablicą, o zadanym w
// konstruktorze rozmiarze, obiektów typu Object.

// Nalezy rowniez zaimplementowac klase Exception, ktorej obiekt jest
// rzucany w drugim bloku try.

// Komentarze, ktore sa w ciele funkcji main pokazuja tekst wypisywany
// w danej liniii.

// Prosze zwrocic uwage na zarzadzanie pamiecia. Tak aby nie bylo
// wyciekow, ani podwojnego usuwania obiektow.

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab08. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Pliku Main.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). Powinien działać dla dowolnej
// liczby przekształceń.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_08

#include"lab08.h"
#include<iostream>

int main ()
{
  try {
    Object first ("first");  // Construct: first
    Exception problem (); // Creating exception
    first.Skip();		
    std::cout <<"this text is invisible"<<std::endl;
    throw "Nothing important";
  }
  catch (...) { }

  try {
    ObjArray* p_array = new ObjArray (3);
    p_array->Set(0, new Object("second")); // Construct: second
    p_array->Set(1, new Object("third"));  // Construct: third
    p_array->Set(2, new Object("forth"));  // Construct: forth
    Exception problem (p_array); // Creating exception
    throw problem;		 // Something has gone wrong
    std::cout <<"this text is invisible too"<<std::endl;
  }
  catch (...) { 
    std::cout<<"Deleting"<<std::endl;
  }
  std::cout<<"The end"<<std::endl;
}
/*
Construct: first
Creating exception
Destruct: first
Construct: second
Construct: third
Construct: forth
Creating exception
Something has gone wrong
Deleting
Destruct: second
Destruct: third
Destruct: forth
The end
*/
