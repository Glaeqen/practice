// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze zaimplementowac klasy File, Read, Write, RWFile, które będą
// służyły do czytania z pliku jak i zapisu do pliku.

// Prosze zwrocic uwage, aby funkcja WriteToFile od razu zapisywala
// dane do pliku, tak by wywolana zaraz po niej funkcja read odczytala
// tekst zapisy przez funkcje WriteToFile.

// Po odkomentowaniu ktorejkolwiek z linii definiujących makra ERROR_
// powinien pojawic sie blad kompilacji.

// Pliku lab07.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab07. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_07

#include"lab07.h"

//#define ERROR_FIRST
//#define ERROR_SECOND
//#define ERROR_THIRD

int main ()
{
  Read read ("read.txt");
  read.Print();			// prints file content on the screen i.e. "Reading test passed."
  File *p_read = &read;
  p_read->PrintInfo();		// prints information about object i.e. "This is read object operating on file: read.txt"

  Write write ("write.txt");
  write.WriteToFile("Test of writing to file successful."); // creates a text file with content: "Test of writing to file successful."
  File *p_write = &write;
  p_write->PrintInfo();
  
  RWFile rwfile ("read_write.txt");
  rwfile.WriteToFile("Test of read and write to file successful.");
  rwfile.Print();
  File *p_rwfile = &rwfile;
  p_rwfile->PrintInfo();
  Write *p_rwwrite = &rwfile;
  p_rwwrite->WriteToFile("Second test");
  Read *p_rwread = &rwfile;
  p_rwread->Print();
  
  
#ifdef ERROR_FIRST
  File generic ("test.txt");
#endif  
#ifdef ERROR_SECOND
  write.Print();
#endif  
#ifdef ERROR_THIRD
  read.WriteToFile("TEST");
#endif  

  return 0;
}
/*
Reading test passed.
This is read object operating on file: read.txt
This is write object operating on file: write.txt
Test of read and write to file successful.
This is read and write object operating on file: read_write.txt
Second test
*/
