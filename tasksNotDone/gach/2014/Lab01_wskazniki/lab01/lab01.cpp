// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze zaimplementowac funkcje CiagArytmetyczny i Wypisz.
// Pierwszy wyraz ciagu arytmetycznego wynosi 0.5.

// Prosze zadbac o dobre zarzadzanie pamiecia.

// UWAGA! Przy rozwiazaniu prosze nie uzywac klas.
// UWAGA! Program mozna kompilowac bez flagi -Wall. 

// Program musi dzialac dla dowolnej liczby wyrazow ciagu.

// Pliku lab01.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab01. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_01


#include"lab01.h"


int main ()
{
  const rozmiar ileWyrazow (12);
  const float roznica = 1.5;
  const wyraz pierwszy = CiagArytmetyczny(ileWyrazow, roznica);
  const wyraz ostatni = pierwszy + ileWyrazow - 1;
  wyraz pierwszy2 = CiagArytmetyczny(ileWyrazow, roznica);
  wyraz ostatni2 = pierwszy2 + ileWyrazow - 1;

  // napisz, ze wypisawane sa wyrazy pierwszego ciagu i wypisz co
  // czwarty wyraz ciagu
 poczatek:
  {
    const int skok = 3;
    Wypisz (skok, pierwszy, ostatni);
  }
  // napisz, ze wypisawane sa wyrazy drugiego ciagu i wypisz co
  // piaty wyraz ciagu
  Wypisz (skok, pierwszy, ostatni);
  Wypisz (skok, pierwszy2, ostatni2);

  // wypisz wyrazy ciagu zaczynajac od pierwszego i konczac na czwartym od konca
  Wypisz (pierwszy, ostatni-3);
  // wypisz wyrazy ciagu zaczynajac od ostatniego i konczac na czwartym
  Wypisz (ostatni, pierwszy+3);

  Wypisz (pierwszy, ostatni, 1000000000); // 0.5 - 17
  Wypisz (pierwszy, ostatni, 10000000000); // 0.5 - 17
  Wypisz (pierwszy, ostatni, 1000000000u); // 17 - 0.5
  Wypisz (pierwszy, ostatni, 10000000000u); // 0.5 - 17
  Wypisz (pierwszy, ostatni, 1000000000L);  // 0.5 - 17
  Wypisz (pierwszy, ostatni, 1000000000.);  // 0.5 - 17
  Wypisz (pierwszy, ostatni, 1000000000.L); // 17 - 05
  Wypisz (pierwszy, ostatni, 1000000000.F); // 0.5 - 17
  
  Wypisz ();

  return 0;
}
/* wynik dzialania programu:
pierwszy
0.5, 5, 9.5, 14
pierwszy
0.5, 6.5, 12.5
drugi
0.5, 6.5, 12.5
0.5, 2, 3.5, 5, 6.5, 8, 9.5, 11, 12.5
17, 15.5, 14, 12.5, 11, 9.5, 8, 6.5, 5
0.5 - 17
0.5 - 17
17 - 0.5
0.5 - 17
0.5 - 17
0.5 - 17
17 - 0.5
0.5 - 17
*/
