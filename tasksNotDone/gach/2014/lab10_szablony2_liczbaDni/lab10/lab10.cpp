// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze napisac szablony obliczajace liczbe dni w miesiacu. Kazdy
// szablon ma dwa argumenty, rok i miesiac (podany jako skrot nazwy
// angielskiej). Obliczanie liczby dni, ma odbywać się na etapie
// kompilaji.

// Przydatna moze okazac sie informacja czy rok est
// przestepny.  Rok jest przestepny gdy dzieli sie (bez reszty) przez
// 400 lub gdy Rok dzieli sie przez 4 lecz nie dzieli sie prze 100.

// Pliku lab10.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie lab10. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_10

#include <iostream>
#include "lab10.h"


int main() {
  using namespace std;

  cout << " liczba dni w styczniu 2014: " << days<2014, Jan>::value << endl; 
  cout << " liczba dni w styczniu 2012: " << days<2012, Jan>::value << endl; 

  cout << " liczba dni w lutym 2014: " << days<2014, Feb>::value << endl; 
  cout << " liczba dni w lutym 2012: " << days<2012, Feb>::value << endl; 


  cout << " liczba dni we wrzesniu 2014: " << days<2014, Sep>::value << endl; 
  cout << " liczba dni we wrzesniu 2012: " << days<2012, Sep>::value << endl; 
}
/* 
 liczba dni w styczniu 2014: 31
 liczba dni w styczniu 2012: 31
 liczba dni w lutym 2014: 28
 liczba dni w lutym 2012: 29
 liczba dni we wrzesniu 2014: 30
 liczba dni we wrzesniu 2012: 30
 */
