// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// --------------- UWAGA ---------------
// *** Nie wolno używać dziedziczenia! ***

// *** Każda funkcja oprócz konstruktorów może być zaimplementowana TYLKO raz. *** 

// *** Nie należy przeładowywać zaimplementowanych przez siebie metod. ***
// --------------- UWAGA ---------------

// Prosze zaimplementowac typ o nazwie Object, ktory bedzie mial
// konstruktor i destruktor wypisujace tekst na ekran oraz funkcje
// print wypisujaca jego nazwe na ekran

// Prosze rowniez zaimplementowac klase, ktora bedzie przechowywala
// wskaznik na obiekt typu Object. Powinien byc to unikatowy wskaznik,
// to znaczy tylko jeden obiekt moze wskazywac na jeden obiekt typu
// Object. Gdy wskaznik jest niszczony rowniez obiekt na ktory
// wskazuje jest niszczony.

// Pliku lab09.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab09. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_09

#include"lab09.h"
#include<iostream>

int main ()
{
  using namespace std;

  UniquePointer<Object> ptr1 = PointerFactory (new Object ("dynamic1"));
  ptr1->print();		// This is dynamic1

  UniquePointer<Object> ptr2 = ptr1;
  ptr2->print();			// This is a pointer to dynamic1


  UniquePointer<OtherObject> ptrOther = PointerFactory (new OtherObject ("otherObject"));
  ptrOther->print();

  return 0;
}
/*
Constructing object dynamic1
This is dynamic1
This is dynamic1
Constructing object otherObject
This is otherObject
Destroying object otherObject
Destroying object dynamic1
*/
