#include<TRandom3.h>
#include<TString.h>
#include<iostream>
#include<fstream>
#include<istream>

using namespace std;

Float_t Normalny (Float_t step, Float_t sigma) 
{
  static TRandom3 random(0);
  return random.Gaus(step, sigma);
}

Float_t Plaski (Float_t step, Float_t sigma) 
{
  static TRandom3 random(0);
  return random.Uniform(step-sigma, step+sigma);
}

Float_t Land (Float_t step, Float_t sigma) 
{
  static TRandom3 random(0);
  return random.Landau(step, sigma);
}


void MakeFile (const TString s_fileName, const Int_t n_rows, const Float_t timeStep, const Float_t timeSigma, const Float_t positionStep, const Float_t positionSigma, Float_t (*randomNumber) (Float_t step, Float_t sigma) = Normalny)
{
  
  ofstream myfile;
  myfile.open(s_fileName.Data());

  Float_t time(0), position(0);
  for (Int_t i = 0; i < n_rows; ++i) {
    myfile<<(time += (*randomNumber)(timeStep, timeSigma) )<<",";
    myfile<<(position += (*randomNumber)(positionStep, positionSigma));
    myfile<<"\n";
  }
  
  myfile.close();
}

void PrintHeaderCol (ofstream &myfile, int n_cols, int i, char &litera, char &jednostka)
{
  // myfile<<"nazwa zmiennej"<<i+1<<"(";
  // print short name (with many letters)
  myfile<<litera++;
}


void MakeFileMultiCol (const TString s_fileName, const Int_t n_rows, const Int_t n_cols, Float_t (*randomNumber) (Float_t step, Float_t sigma) = Normalny)
{
  ofstream myfile;
  myfile.open(s_fileName.Data());

  TRandom3 random (0);
  
  // generate steps and initial values for each column
  Float_t step[n_cols], stepStd[n_cols], val[n_cols];
  for (Int_t i = 0; i < n_cols; ++i) {
    step[i] = random.Uniform(1, 5);
    stepStd[i] = random.Uniform(0, step[i]);
    val[i] = random.Uniform(1, 10);
  }

  
  // fill header
  // myfile<<n_cols<<","<<n_rows<<"\n";
  // char litera = 'D';
  // char jednostka = 'a';
  // PrintHeaderCol(myfile, n_cols, 0, litera, jednostka);
  // for (Int_t i = 1; i < n_cols; ++i) {
  //   myfile<<",";
  //   PrintHeaderCol(myfile, n_cols, i, litera, jednostka);
  // }
  // myfile<<"\n";

  // fill data
  for (Int_t i = 0; i < n_rows; ++i) {
    myfile<<(*randomNumber)(step[0], stepStd[0]) );
    for (Int_t j = 1; j < n_cols; ++j)
      myfile<<","<<(*randomNumber)(step[j], stepStd[j]);
    myfile<<"\n";
  }
  
  myfile.close();
}

void GenerateData ()
{
  MakeFileMultiCol ("Normalny.csv", 123, 1);
  MakeFileMultiCol ("Plaski.csv", 84, 1, Plaski);
}
