#pragma once
#include "action.h"
#include "itr.h"
namespace numutils
{

//////////////////////

class sum : public action
{
public:
	explicit sum(){};
	~sum(){};
	action& act(itr it)
	{
		float v=0;
		for(int i=0; i<it.size; i++)
		{
			v+=it.tab[i];
		}
		value=v;
		return *this;
	};
	sum& operator()(int val)
	{
		value+=val;
		return *this;
	};
	float result()
	{
		return value;
	};
	float value;
};

/////////////////////

class average: public action
{
public:
	average(){};
	~average(){};
	action& act(itr it)
	{
		float v=0;
		for(int i=0; i<it.size; i++)
		{
			v+=it.tab[i];
		}
		value = v/it.size;
		return *this;
	};
	float result()
	{
		return value;
	};
private:
	float value;
};

//////////////////////

class minmax: public action
{
public:
	minmax(){};
	~minmax(){};
	action& act(itr it)
	{

		float max = 0;
/*		for(; it ; ++it)
		{
			//std::cout <<*it;
			if(max<*it)
				max = *it;
		}*/
		_min =0;
		_max = max;
		return *this;

	};
	float min() const
	{
		return _min;
	};

	float max() const
	{
		return _max;
	};

private:
	float _min;
	float _max;
};
}